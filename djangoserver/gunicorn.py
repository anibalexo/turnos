# Gunicorn File

bind = '0.0.0.0:8000'
reload = True
deamon = True
access_log = 'var/log/gunicorn/gunicorn-access.log'
error_log = 'var/log/gunicorn/gunicorn-error.log'
log_level = 'info'