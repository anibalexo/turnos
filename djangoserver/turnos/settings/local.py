from .base import *

SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY")
DEBUG = True
ALLOWED_HOSTS = ["*"]

INSTALLED_APPS = INSTALLED_APPS + [
    'debug_toolbar',
]

MIDDLEWARE = MIDDLEWARE + [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

ANTECESOR_BASE_DIR = Path(BASE_DIR).ancestor(1)
STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(ANTECESOR_BASE_DIR, 'static'),)
STATIC_ROOT = os.path.join(ANTECESOR_BASE_DIR, 'staticfiles')

#RUTA CSS REPORTES
STATIC = os.path.join(ANTECESOR_BASE_DIR, 'static')

#CONFIG DJANGO TOOLBAR DEBUG
import socket
hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
INTERNAL_IPS = [ip[:-1] + '1' for ip in ips] + ['127.0.0.1', '10.0.2.2']
DEBUG_TOOLBAR_CONFIG = {
    'RESULTS_CACHE_SIZE': 3,
    'SHOW_COLLAPSED': True,
    'SQL_WARNING_THRESHOLD': 100,
}

