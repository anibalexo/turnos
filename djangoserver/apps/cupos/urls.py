from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import GestionCuposTemplateView

urlpatterns = [
    path('gestion/', login_required(GestionCuposTemplateView.as_view()), name='cupo-gestion'),

]
