from django.views.generic import TemplateView

from ..capital.models.periodo import Periodo


class GestionCuposTemplateView(TemplateView):
    template_name = 'cupos/gestion_cupos_metas.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['lista_periodos'] = Periodo.objects.using('capital').all()
        return context

