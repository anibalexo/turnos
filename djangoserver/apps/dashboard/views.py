from django.db.models import Count
from django.utils.datetime_safe import date
from django.views.generic import TemplateView

from ..capital.models import Turno
from ..usuarios.models import Usuario


class DashboardTemplateView(TemplateView):
    template_name = 'dashboard/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        usuario = Usuario.objects.get(id=self.request.user.id)
        context['fecha_actual'] = date.today()
        context['estadisticas'] = self.get_estadisticas(date.today(), usuario.sucursal.codigo)
        return context

    def get_estadisticas(self, date, sucursal):
        data_estadisticas = Turno.objects.using('capital').filter(fecha_entrd=date, sucu_crea=sucursal)\
                                         .values('estado_turno', 'tipo_clie')\
                                         .annotate(Count('estado_turno')).order_by()
        turnos_abiertos = 0
        turnos_cerrados = 0
        turnos_anulados = 0
        turnos_pendientes = 0
        for item in data_estadisticas:
            if item['tipo_clie'] == 'DIST':
                turnos_abiertos += item['estado_turno__count']
                if item['estado_turno'] == 'E':
                    turnos_pendientes = item['estado_turno__count']
                elif item['estado_turno'] == 'A':
                    turnos_anulados = item['estado_turno__count']
                elif item['estado_turno'] == 'S':
                    turnos_cerrados = item['estado_turno__count']
        result = {}
        result['total_abiertos'] = turnos_abiertos
        result['total_cerrados'] = turnos_cerrados
        result['total_anulados'] = turnos_anulados
        result['total_pendientes'] = turnos_pendientes
        return result
