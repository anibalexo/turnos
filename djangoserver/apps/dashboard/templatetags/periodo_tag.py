import datetime

from django import template
from ...capital.models.periodo import Periodo

register = template.Library()

@register.simple_tag(takes_context=True)
def periodo_actual(context):
    date_today = datetime.date.today()
    try:
        data = Periodo.objects.using('capital').get(codi_admi_empr_fina='LOJAG', inve_fech_ini__lte=date_today,
                                                    inve_fech_fin__gte=date_today)
    except:
        data = Periodo.objects.using('capital').filter(codi_admi_empr_fina='LOJAG').order_by('-inve_fech_ini')[0]
    return "<span class='badge badge-secondary badge-pill info-periodo'>{}</span>".format(data.inve_codi_desc)
