import datetime

import django.db as db
from django.db import connections
from rest_framework.exceptions import ValidationError

from ..models import Cliente, Tarifa


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def actualiza_meta_distribuidor(pk_meta, pk_periodo, pk_cliente, pk_tarifa, cantidad, min_domestico):
    """
    Permite actualizar meta a distribuidor
    :return: instancia meta actualizado
    """
    cursor = connections['capital'].cursor()
    exist_cliente = Cliente.objects.using('capital').filter(codi_inve_clie=pk_cliente).exists()
    if not exist_cliente:
        raise ValidationError("No existe el cliente")

    exist_tarifa = Tarifa.objects.using('capital').filter(codi_inve_tari=pk_tarifa).exists()
    if not exist_tarifa:
        raise ValidationError("No existe la tarifa")
    resultado = False
    try:
        parametros = {'cantidad': cantidad, 'periodo': pk_periodo, 'pk_meta': pk_meta, 'cliente': pk_cliente, 'tarifa': pk_tarifa, 'min_domestico': min_domestico}
        sql = """
        UPDATE INVE_METAV_GLP_DET_DAT SET CUPO_CANT_PROD = %(cantidad)s, SALDO_CANT_PROD = %(cantidad)s, NUME_INVE_PROD_EQU0 = %(min_domestico)s 
        WHERE INVE_CODI_META = %(periodo)s AND INVE_CODI_DET = %(pk_meta)s AND CODI_INVE_CLIE = %(cliente)s AND CODI_INVE_TARI = %(tarifa)s """ % parametros
        cursor.execute(sql)

        sql_verificar = """
        SELECT * FROM INVE_METAV_GLP_DET_DAT WHERE INVE_CODI_META = %(periodo)s AND INVE_CODI_DET = %(pk_meta)s 
        AND CODI_INVE_CLIE = %(cliente)s AND CODI_INVE_TARI = %(tarifa)s
        """ % parametros
        cursor.execute(sql_verificar)
        row = dictfetchall(cursor)
        if len(row) == 1:
            resultado = True

    finally:
        cursor.close()
    return resultado


def actualiza_cupo_distribuidor(pk_cupo, pk_periodo, pk_cliente, pk_tarifa, cantidad, min_domestico):
    """
    Permite actualizar cupo a distribuidor
    :return: instancia cupo actualizado
    """
    cursor = connections['capital'].cursor()
    exist_cliente = Cliente.objects.using('capital').filter(codi_inve_clie=pk_cliente).exists()
    if not exist_cliente:
        raise ValidationError("No existe el cliente")

    exist_tarifa = Tarifa.objects.using('capital').filter(codi_inve_tari=pk_tarifa).exists()
    if not exist_tarifa:
        raise ValidationError("No existe la tarifa")
    resultado = False
    try:
        parametros = {'cantidad': cantidad, 'periodo': pk_periodo, 'pk_cupo': pk_cupo, 'cliente': pk_cliente, 'tarifa': pk_tarifa, 'min_domestico': min_domestico}
        sql = """
        UPDATE INVE_CUPOS_GLP_DET_DAT SET CUPO_CANT_PROD = %(cantidad)s, SALDO_CANT_PROD = %(cantidad)s, NUME_INVE_PROD_EQU0 = %(min_domestico)s  
        WHERE INVE_CODI_CUPO = %(periodo)s AND INVE_CODI_DET = %(pk_cupo)s AND CODI_INVE_CLIE = %(cliente)s AND CODI_INVE_TARI = %(tarifa)s """ % parametros
        cursor.execute(sql)

        sql_verificar = """
        SELECT * FROM INVE_CUPOS_GLP_DET_DAT WHERE INVE_CODI_CUPO = %(periodo)s AND INVE_CODI_DET = %(pk_cupo)s 
        AND CODI_INVE_CLIE = %(cliente)s AND CODI_INVE_TARI = %(tarifa)s
        """ % parametros
        cursor.execute(sql_verificar)
        row = dictfetchall(cursor)
        if len(row) == 1:
            resultado = True

    finally:
        cursor.close()
    return resultado


def actualiza_saldos_cliente():
    """
    Permite actualizar saldos de facturas pendientes
    :return: string OK o ERROR con descripcion
    """
    cursor = connections['capital'].cursor()
    try:
        cursor.callproc("LOJAGASNIIF.genera_cxc_pendientes", ['LOJAG', 'DIS'])
        return "OK"
    except db.Error as er:
        return "ERROR: %s " % er
    finally:
        cursor.close()


def get_saldo_cliente(empresa, fecha, cliente_id, producto_id, tarifa_id, sucursal_id):
    """
    Permite obtener el saldo del cliente
    desde una función sql de oracle
    :param empresa: empresa actual
    :param fecha: fecha actual
    :param cliente_id: identificador de cliente
    :param producto_id: producto a consultar
    :param tarifa_id: tarifa de cliente
    :param sucursal_id: sucursal del cliente
    """
    cursor = connections['capital'].cursor()
    exist_cliente = Cliente.objects.using('capital').filter(codi_inve_clie=cliente_id).exists()
    if not exist_cliente:
        raise ValidationError("No existe el cliente en CAPITAL")

    exist_tarifa = Tarifa.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', codi_inve_tari=tarifa_id).exists()
    if not exist_tarifa:
        raise ValidationError("No existe la tarifa en CAPITAL")

    try:
        cursor.execute("SELECT SALDO_CUPOS_GLP(%s, %s, %s, %s, %s, %s) AS SALDO FROM DUAL",
                       [empresa, fecha, cliente_id, producto_id, tarifa_id, sucursal_id])
        row = cursor.fetchone()
    finally:
        cursor.close()
    return row[0]


def get_turnos_pendientes(empresa, fecha, cliente_id, producto_id, tarifa_id, sucursal_id):
    """
    Permite obtener los turnos pendientes de cerrar
    :param empresa: empresa actual
    :param fecha: fecha actual
    :param cliente_id: identificador de cliente
    :param producto_id: identificador de producto
    :param tarifa_id: indetificador de tarifa
    :param sucursal_id: identificador de sucursal
    :return:
    """
    cursor = connections['capital'].cursor()
    exist_cliente = Cliente.objects.using('capital').filter(codi_inve_clie=cliente_id).exists()
    if not exist_cliente:
        raise ValidationError("No existe el cliente en CAPITAL")

    exist_tarifa = Tarifa.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', codi_inve_tari=tarifa_id).exists()
    if not exist_tarifa:
        raise ValidationError("No existe la tarifa en CAPITAL")

    try:
        cursor.execute("SELECT PEDIDOS_PEDIENTES_GLP(%s, %s, %s, %s, %s, %s) AS PENDIENTES FROM DUAL",
                       [empresa, fecha, cliente_id, producto_id, tarifa_id, sucursal_id])
        row = cursor.fetchone()
    finally:
        cursor.close()
    return row[0]


def get_reporte_stock_disponible_meta(periodo):
    """
    Obtiene stock disponible a distribuidores
    controlados por meta
    :param periodo: instancia de periodo a buscar
    :return: queryset
    """
    cursor = connections['capital'].cursor()
    try:
        parametros = {"fecha_inicio": periodo.inve_fech_ini, "fecha_fin": periodo.inve_fech_fin, "periodo_id": periodo.inve_codi_cupo}
        sql = """
        SELECT 
            X.CODI_INVE_CLIE, C.NOMB_INVE_CLIE, X.CODI_INVE_TARI, T.NOMB_INVE_TARI, CODI_INVE_TIPO_PROD, NVL(SUM(CUPO),0) CUPO, 
            NVL(SUM(CUPO),0) - NVL(SUM(CANTIDAD),0) DESPACHOS, NVL(SUM(CANTIDAD),0) SALDO,
            ROUND((1-(NVL(SUM(CANTIDAD),0)/DECODE(NVL(SUM(CUPO),0),0,1,NVL(SUM(CUPO),0))))*100,2) PORC_DESPACHO, 
            TO_CHAR(SYSDATE,'YYYY-MM-DD HH24:MI:SS') FECHAREPORTE 
        FROM(
            SELECT B.CODI_INVE_CLIE,B.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD, SUM(B.CUPO_CANT_PROD) CUPO, SUM(B.CUPO_CANT_PROD) CANTIDAD 
            FROM INVE_METAV_GLP_DAT A, INVE_METAV_GLP_DET_DAT B, INVE_CILINDROS_DAT C 
            WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' AND A.CODI_ADMI_EMPR_FINA = B.CODI_ADMI_EMPR_FINA 
            AND B.CODI_ADMI_EMPR_FINA = C.CODI_ADMI_EMPR_FINA AND A.INVE_CODI_META = B.INVE_CODI_META AND B.CODI_INVE_PROD = C.CODI_INVE_PROD 
            AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY-MM-DD') AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY/MM/DD') 
            AND C.CODI_INVE_TIPO_PRES = 'GLP' AND C.CODI_INVE_TIPO_PROD = 'DOM' 
            GROUP BY B.CODI_INVE_CLIE,B.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD
            UNION ALL 
            SELECT D.CODI_INVE_CLIE,M.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD, SUM(0) CUPO,SUM(M.UNID_INVE_MOVI)*-1 TOTAL_REAL 
            FROM INVE_DOCUMENTOS_DAT D, INVE_MOVIMIENTOS_DAT M, INVE_CILINDROS_DAT C 
            WHERE D.CODI_ADMI_EMPR_FINA = 'LOJAG' AND D.CODI_ADMI_EMPR_FINA = M.CODI_ADMI_EMPR_FINA AND M.CODI_ADMI_EMPR_FINA = C.CODI_ADMI_EMPR_FINA 
            AND D.CODI_INVE_TIPO_DOCU = M.CODI_INVE_TIPO_DOCU AND D.CODI_INVE_DOCU = M.CODI_INVE_DOCU AND M.CODI_INVE_PROD = C.CODI_INVE_PROD 
            AND C.CODI_INVE_TIPO_PRES = 'GLP' AND C.CODI_INVE_TIPO_PROD = 'DOM' 
            AND D.CODI_INVE_TIPO_DOCU IN (SELECT T.CODI_INVE_TIPO_DOCU FROM INVE_TIPOS_DOCUMENTOS_REF T WHERE T.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                        AND T.ASOC_INVE_TIPO_DOCU = 'C' AND T.CLAS_INVE_TIPO_DOCU = 6 AND T.ASIE_CONT_INVE_TIPO_DOCU = 1) 
            AND D.CODI_ADMI_ESTA IN ('P','O') 
            AND D.FECH_INVE_DOCU >= (SELECT A.INVE_FECH_INI FROM INVE_METAV_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                    AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY/MM/DD') 
                                    AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY/MM/DD')) 
            AND D.FECH_INVE_DOCU <= (SELECT A.INVE_FECH_FIN FROM INVE_METAV_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                    AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY/MM/DD') 
                                    AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY/MM/DD')) 
            AND D.CODI_INVE_CLIE NOT IN (SELECT CD2.CODI_INVE_CLIE FROM INVE_CUPOS_GLP_DET_DAT CD2 WHERE CD2.INVE_CODI_CUPO = '%(periodo_id)s' 
                                         AND CD2.CODI_INVE_CLIE = D.CODI_INVE_CLIE AND CD2.CODI_INVE_TARI = M.CODI_INVE_TARI)
            GROUP BY D.CODI_INVE_CLIE,M.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD 
            UNION ALL           
            SELECT T.CODI_INVE_CLIE,T.CODI_INVE_TARI_DOM15,'DOM' CODI_INVE_TIPO_PROD, SUM(0) CUPO, SUM(T.CARG_DOM_15KG) * -1 TOTAL_PEDIDO15 
            FROM TBL_TURNOS_DISTRIBUIDOR T WHERE T.CODI_ADMI_EMPR_FINA = 'LOJAG' AND ((T.ESTADO_TURNO = 'E' 
            AND T.TIPO_CLIE = 'DIST') OR (T.ESTADO_TURNO = 'S' AND T.TIPO_CLIE = 'EMPL'))
            AND T.FECHA_ENTRD >= (SELECT A.INVE_FECH_INI FROM INVE_METAV_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                  AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY/MM/DD') 
                                  AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY/MM/DD')) 
            AND T.FECHA_ENTRD <= (SELECT A.INVE_FECH_FIN FROM INVE_METAV_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                  AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY/MM/DD') 
                                  AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY/MM/DD')) 
            AND T.CODI_INVE_CLIE NOT IN (SELECT CD2.CODI_INVE_CLIE FROM INVE_CUPOS_GLP_DET_DAT CD2 WHERE CD2.INVE_CODI_CUPO = '%(periodo_id)s' 
                                         AND CD2.CODI_INVE_CLIE = T.CODI_INVE_CLIE AND CD2.CODI_INVE_TARI = T.CODI_INVE_TARI_DOM15)
            GROUP BY T.CODI_INVE_CLIE,T.CODI_INVE_TARI_DOM15 
        ) X, INVE_CLIENTES_DAT C, INVE_TARIFAS_DAT T 
        WHERE X.CODI_INVE_CLIE = C.CODI_INVE_CLIE AND X.CODI_INVE_TARI = T.CODI_INVE_TARI 
        GROUP BY X.CODI_INVE_CLIE,C.NOMB_INVE_CLIE,X.CODI_INVE_TARI,T.NOMB_INVE_TARI,CODI_INVE_TIPO_PROD 
        ORDER BY C.NOMB_INVE_CLIE        
        """ % parametros
        cursor.execute(sql)
        rows = dictfetchall(cursor)
        for rw in rows:
            try:
                today = datetime.date.today()
                fecha = today.strftime("%Y-%m-%d")
                cliente_id = rw['CODI_INVE_CLIE']
                producto_id = "P-CILI-D15"
                tarifa_id = rw['CODI_INVE_TARI']
                cupo = int(rw['CUPO'])
                saldo = int(rw['SALDO'])
                sucursal_id = 0
                total_abiertas = get_turnos_pendientes("LOJAG", fecha, cliente_id, producto_id, tarifa_id, sucursal_id)
                rw['ORDENES_ABIERTAS'] = total_abiertas
                rw['SALDO_REAL'] = cupo - (cupo - saldo) - total_abiertas
                rw['PORC_15'] = cupo * 15 / 100
            except:
                rw['ORDENES_ABIERTAS'] = 0
                rw['SALDO_REAL'] = 0
                rw['PORC_15'] = 0

        data = {"fecha": rows[0]['FECHAREPORTE'], "registros": rows}
    finally:
        cursor.close()

    return data


def get_reporte_stock_disponible_cupo(periodo):
    """
    Obtiene stock disponible a distribuidores
    controlados por cupo
    :param periodo: instancia de periodo a buscar
    :return: queryset
    """
    cursor = connections['capital'].cursor()
    try:
        parametros = {"fecha_inicio": periodo.inve_fech_ini, "fecha_fin": periodo.inve_fech_fin, "periodo_id": periodo.inve_codi_cupo}
        sql = """
        SELECT X.CODI_INVE_CLIE, C.NOMB_INVE_CLIE, X.CODI_INVE_TARI, T.NOMB_INVE_TARI, CODI_INVE_TIPO_PROD, NVL(SUM(CUPO),0) CUPO, 
        NVL(SUM(CUPO),0) - NVL(SUM(CANTIDAD),0) DESPACHOS, NVL(SUM(CANTIDAD),0) SALDO, 
        ROUND((1-(NVL(SUM(CANTIDAD),0)/DECODE(NVL(SUM(CUPO),0),0,1, NVL(SUM(CUPO),0))))*100,2) PORC_DESPACHO, 
        TO_CHAR(SYSDATE,'DD-MM-YYYY HH24:MI:SS') FECHAREPORTE   
        FROM(
            SELECT B.CODI_INVE_CLIE,B.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD, SUM(B.CUPO_CANT_PROD) CUPO, SUM(B.CUPO_CANT_PROD) CANTIDAD 
            FROM INVE_CUPOS_GLP_DAT A, INVE_CUPOS_GLP_DET_DAT B, INVE_CILINDROS_DAT C WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
            AND A.CODI_ADMI_EMPR_FINA = B.CODI_ADMI_EMPR_FINA AND B.CODI_ADMI_EMPR_FINA = C.CODI_ADMI_EMPR_FINA 
            AND A.INVE_CODI_CUPO = B.INVE_CODI_CUPO AND B.CODI_INVE_PROD = C.CODI_INVE_PROD  AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY-MM-DD') 
            AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY-MM-DD') AND C.CODI_INVE_TIPO_PRES = 'GLP' 
            AND B.CUPO_CANT_PROD >= 0 AND B.SALDO_CANT_PROD >= 0
            AND C.CODI_INVE_TIPO_PROD = 'DOM' GROUP BY B.CODI_INVE_CLIE, B.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD 
            UNION ALL
            SELECT D.CODI_INVE_CLIE,M.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD, SUM(0) CUPO,SUM(M.UNID_INVE_MOVI)*-1 TOTAL_REAL 
            FROM INVE_DOCUMENTOS_DAT D, INVE_MOVIMIENTOS_DAT M, INVE_CILINDROS_DAT C WHERE D.CODI_ADMI_EMPR_FINA = 'LOJAG' 
            AND D.CODI_ADMI_EMPR_FINA = M.CODI_ADMI_EMPR_FINA AND M.CODI_ADMI_EMPR_FINA = C.CODI_ADMI_EMPR_FINA 
            AND D.CODI_INVE_TIPO_DOCU = M.CODI_INVE_TIPO_DOCU AND D.CODI_INVE_DOCU = M.CODI_INVE_DOCU AND M.CODI_INVE_PROD = C.CODI_INVE_PROD 
            AND C.CODI_INVE_TIPO_PRES = 'GLP' AND C.CODI_INVE_TIPO_PROD = 'DOM' AND D.CODI_ADMI_ESTA IN ('P','O') 
            AND D.CODI_INVE_TIPO_DOCU IN (SELECT T.CODI_INVE_TIPO_DOCU FROM INVE_TIPOS_DOCUMENTOS_REF T WHERE T.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                          AND T.ASOC_INVE_TIPO_DOCU = 'C' AND T.CLAS_INVE_TIPO_DOCU = 6 AND T.ASIE_CONT_INVE_TIPO_DOCU = 1) 
            AND D.FECH_INVE_DOCU >= (SELECT A.INVE_FECH_INI FROM INVE_CUPOS_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                    AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY-MM-DD') 
                                    AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY-MM-DD')) 
            AND D.FECH_INVE_DOCU <= (SELECT A.INVE_FECH_FIN FROM INVE_CUPOS_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                    AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY-MM-DD') 
                                    AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY-MM-DD')) 
            AND D.CODI_INVE_CLIE NOT IN (SELECT CD2.CODI_INVE_CLIE FROM INVE_METAV_GLP_DET_DAT CD2 WHERE CD2.INVE_CODI_META = '%(periodo_id)s' 
                                         AND CD2.CODI_INVE_CLIE = D.CODI_INVE_CLIE AND CD2.CODI_INVE_TARI = M.CODI_INVE_TARI)
            GROUP BY D.CODI_INVE_CLIE,M.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD 
            UNION ALL 
            SELECT T.CODI_INVE_CLIE,T.CODI_INVE_TARI_DOM15,'DOM' CODI_INVE_TIPO_PROD, SUM(0) CUPO, SUM(T.CARG_DOM_15KG) * -1 TOTAL_PEDIDO15 
            FROM TBL_TURNOS_DISTRIBUIDOR T WHERE T.CODI_ADMI_EMPR_FINA = 'LOJAG' AND T.ESTADO_TURNO = 'E' 
            AND T.FECHA_ENTRD >= (SELECT A.INVE_FECH_INI FROM INVE_CUPOS_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                 AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY-MM-DD') 
                                 AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY-MM-DD')) 
            AND T.FECHA_ENTRD <= (SELECT A.INVE_FECH_FIN FROM INVE_CUPOS_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                 AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY-MM-DD') 
                                 AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY-MM-DD')) 
            AND T.CODI_INVE_CLIE NOT IN (SELECT CD2.CODI_INVE_CLIE FROM INVE_METAV_GLP_DET_DAT CD2 WHERE CD2.INVE_CODI_META = '%(periodo_id)s' 
                                         AND CD2.CODI_INVE_CLIE = T.CODI_INVE_CLIE AND CD2.CODI_INVE_TARI = T.CODI_INVE_TARI_DOM15) 
            GROUP BY T.CODI_INVE_CLIE, T.CODI_INVE_TARI_DOM15 
        ) X, INVE_CLIENTES_DAT C, INVE_TARIFAS_DAT T 
        WHERE X.CODI_INVE_CLIE = C.CODI_INVE_CLIE AND X.CODI_INVE_TARI = T.CODI_INVE_TARI 
        GROUP BY X.CODI_INVE_CLIE, C.NOMB_INVE_CLIE, X.CODI_INVE_TARI, T.NOMB_INVE_TARI, CODI_INVE_TIPO_PROD 
        ORDER BY C.NOMB_INVE_CLIE """ % parametros
        cursor.execute(sql)
        rows = dictfetchall(cursor)
        for rw in rows:
            try:
                today = datetime.date.today()
                fecha = today.strftime("%Y-%m-%d")
                cliente_id = rw['CODI_INVE_CLIE']
                producto_id = "P-CILI-D15"
                tarifa_id = rw['CODI_INVE_TARI']
                cupo = int(rw['CUPO'])
                saldo = int(rw['SALDO'])
                sucursal_id = 0
                total_abiertas = get_turnos_pendientes("LOJAG", fecha, cliente_id, producto_id, tarifa_id, sucursal_id)
                rw['ORDENES_ABIERTAS'] = total_abiertas
                rw['SALDO_REAL'] = cupo - (cupo - saldo) - total_abiertas
                rw['PORC_15'] = cupo * 15 / 100
            except:
                rw['ORDENES_ABIERTAS'] = 0
                rw['SALDO_REAL'] = 0
                rw['PORC_15'] = 0

        data = {"fecha": rows[0]['FECHAREPORTE'], "registros": rows}
    finally:
        cursor.close()

    return data


def get_reporte_stock_minmax_cupo(periodo):
    """
    Obtiene stock disponible a distribuidores
    controlados por cupo
    :param periodo: instancia de periodo a buscar
    :return: queryset
    """
    cursor = connections['capital'].cursor()
    try:
        parametros = {"fecha_inicio": periodo.inve_fech_ini, "fecha_fin": periodo.inve_fech_fin, "periodo_id": periodo.inve_codi_cupo}
        sql = """
        SELECT X.CODI_INVE_CLIE, C.NOMB_INVE_CLIE, X.CODI_INVE_TARI, T.NOMB_INVE_TARI, CODI_INVE_TIPO_PROD,
               NVL(SUM(CUPO),0) CUPO, NVL(SUM(CUPO),0) - NVL(SUM(CANTIDAD),0) DESPACHOS, NVL(SUM(CANTIDAD),0) SALDO,
               ROUND((1-(NVL(SUM(CANTIDAD),0)/DECODE(NVL(SUM(CUPO),0),0,1,NVL(SUM(CUPO),0))))*100,2) PORC_DESPACHO,
               NVL(SUM(EQUIV_MIN_IND),0) EQUIV_MIN_IND, SUM(CODI_TARI_IND) CODI_TARI_IND,
               DECODE(SUM(EQUIV_MIN_IND),0,0,ROUND(NVL(SUM(CUPO),0)/NVL(SUM(EQUIV_MIN_IND),1))) NRO_CIL_IND, 
               VENTAS_GLP_SEGMENTO('LOJAG',TO_CHAR(SYSDATE,'YYYY-MM-DD'),X.CODI_INVE_CLIE,'IND',SUM(CODI_TARI_IND),20) REAL_IND,
               TO_CHAR(SYSDATE,'DD-MM-YYYY HH24:MI:SS') FECHAREPORTE 
        FROM (SELECT B.CODI_INVE_CLIE,B.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD, SUM(B.CUPO_CANT_PROD) CUPO, SUM(B.CUPO_CANT_PROD) CANTIDAD, SUM(NUME_INVE_PROD_EQU0) EQUIV_MIN_IND, SUM(FACT_CONV_PROD_EQU0) CODI_TARI_IND
              FROM INVE_CUPOS_GLP_DAT A, INVE_CUPOS_GLP_DET_DAT B, INVE_CILINDROS_DAT C WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' AND A.CODI_ADMI_EMPR_FINA = B.CODI_ADMI_EMPR_FINA 
              AND B.CODI_ADMI_EMPR_FINA = C.CODI_ADMI_EMPR_FINA 
              AND A.INVE_CODI_CUPO = B.INVE_CODI_CUPO AND B.CODI_INVE_PROD = C.CODI_INVE_PROD 
              AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY-MM-DD') AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY-MM-DD') AND C.CODI_INVE_TIPO_PRES = 'GLP' 
              AND C.CODI_INVE_TIPO_PROD = 'DOM' 
              AND B.CUPO_CANT_PROD >= 0
              GROUP BY B.CODI_INVE_CLIE,B.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD                                                                                                                         
              UNION ALL                                                                                                                         
              SELECT D.CODI_INVE_CLIE,M.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD, SUM(0) CUPO,SUM(M.UNID_INVE_MOVI)*-1 TOTAL_REAL,0 EQUIV_MIN_IND,0 CODI_TARI_IND 
              FROM INVE_DOCUMENTOS_DAT D, INVE_MOVIMIENTOS_DAT M, INVE_CILINDROS_DAT C WHERE D.CODI_ADMI_EMPR_FINA = 'LOJAG' AND D.CODI_ADMI_EMPR_FINA = M.CODI_ADMI_EMPR_FINA 
              AND M.CODI_ADMI_EMPR_FINA = C.CODI_ADMI_EMPR_FINA AND D.CODI_INVE_TIPO_DOCU = M.CODI_INVE_TIPO_DOCU AND D.CODI_INVE_DOCU = M.CODI_INVE_DOCU 
              AND M.CODI_INVE_PROD = C.CODI_INVE_PROD AND C.CODI_INVE_TIPO_PRES = 'GLP' AND C.CODI_INVE_TIPO_PROD = 'DOM' 
              AND D.CODI_INVE_TIPO_DOCU IN (SELECT T.CODI_INVE_TIPO_DOCU FROM INVE_TIPOS_DOCUMENTOS_REF T WHERE T.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                            AND T.ASOC_INVE_TIPO_DOCU = 'C' AND T.CLAS_INVE_TIPO_DOCU = 6 AND T.ASIE_CONT_INVE_TIPO_DOCU = 1) 
              AND D.CODI_ADMI_ESTA IN ('P','O') 
              AND D.FECH_INVE_DOCU >= (SELECT A.INVE_FECH_INI FROM INVE_CUPOS_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                       AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY-MM-DD') 
                                       AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY-MM-DD')) 
              AND D.FECH_INVE_DOCU <= (SELECT A.INVE_FECH_FIN FROM INVE_CUPOS_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                       AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY-MM-DD') 
                                       AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY-MM-DD')) 
              AND D.CODI_INVE_CLIE NOT IN (SELECT CD2.CODI_INVE_CLIE FROM INVE_METAV_GLP_DET_DAT CD2 WHERE CD2.INVE_CODI_META = '%(periodo_id)s' 
                                           AND CD2.CODI_INVE_CLIE = D.CODI_INVE_CLIE AND CD2.CODI_INVE_TARI = M.CODI_INVE_TARI)
              GROUP BY D.CODI_INVE_CLIE,M.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD                                                                                                 
              UNION ALL                                                                                                 
              SELECT T.CODI_INVE_CLIE,T.CODI_INVE_TARI_DOM15,'DOM' CODI_INVE_TIPO_PROD, SUM(0) CUPO, SUM(T.CARG_DOM_15KG) * -1 TOTAL_PEDIDO15,0 EQUIV_MIN_IND,0 CODI_TARI_IND
              FROM TBL_TURNOS_DISTRIBUIDOR T WHERE T.CODI_ADMI_EMPR_FINA = 'LOJAG' AND T.ESTADO_TURNO = 'E' 
              AND T.FECHA_ENTRD >= (SELECT A.INVE_FECH_INI FROM INVE_CUPOS_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                    AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY-MM-DD') 
                                    AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY-MM-DD')) 
              AND T.FECHA_ENTRD <= (SELECT A.INVE_FECH_FIN FROM INVE_CUPOS_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                    AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY-MM-DD') 
                                    AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY-MM-DD')) 
              AND T.CODI_INVE_CLIE NOT IN (SELECT CD2.CODI_INVE_CLIE FROM INVE_METAV_GLP_DET_DAT CD2 WHERE CD2.INVE_CODI_META = '%(periodo_id)s'  
                                           AND CD2.CODI_INVE_CLIE = T.CODI_INVE_CLIE AND CD2.CODI_INVE_TARI = T.CODI_INVE_TARI_DOM15)                        
              GROUP BY T.CODI_INVE_CLIE,T.CODI_INVE_TARI_DOM15                 
        ) X, INVE_CLIENTES_DAT C, INVE_TARIFAS_DAT T                 
        WHERE X.CODI_INVE_CLIE = C.CODI_INVE_CLIE 
        AND X.CODI_INVE_TARI = T.CODI_INVE_TARI 
        GROUP BY X.CODI_INVE_CLIE,C.NOMB_INVE_CLIE,X.CODI_INVE_TARI,T.NOMB_INVE_TARI,CODI_INVE_TIPO_PROD 
        ORDER BY C.NOMB_INVE_CLIE        
        """ % parametros
        cursor.execute(sql)
        rows = dictfetchall(cursor)
        for rw in rows:
            real_ind = int(rw['REAL_IND'])
            min_ind = int(rw['NRO_CIL_IND'])
            rw['SALDO_IND'] = real_ind - min_ind

        data = {"fecha": rows[0]['FECHAREPORTE'], "registros": rows}
    finally:
        cursor.close()

    return data


def get_reporte_stock_minmax_meta(periodo):
    """
    Obtiene stock disponible a distribuidores
    controlados por meta
    :param periodo: instancia de periodo a buscar
    :return: queryset
    """
    cursor = connections['capital'].cursor()
    try:
        parametros = {"fecha_inicio": periodo.inve_fech_ini, "fecha_fin": periodo.inve_fech_fin, "periodo_id": periodo.inve_codi_cupo}
        sql = """
        SELECT X.CODI_INVE_CLIE, C.NOMB_INVE_CLIE, X.CODI_INVE_TARI, T.NOMB_INVE_TARI, CODI_INVE_TIPO_PROD, NVL(SUM(CUPO),0) CUPO, 
               NVL(SUM(CUPO),0) - NVL(SUM(CANTIDAD),0) DESPACHOS, NVL(SUM(CANTIDAD),0) SALDO,
               ROUND((1-(NVL(SUM(CANTIDAD),0)/DECODE(NVL(SUM(CUPO),0),0,1,NVL(SUM(CUPO),0))))*100,2) PORC_DESPACHO, 
               NVL(SUM(EQUIV_MIN_IND),0) EQUIV_MIN_IND, SUM(CODI_TARI_IND) CODI_TARI_IND,
               DECODE(SUM(EQUIV_MIN_IND),0,0,ROUND(NVL(SUM(CUPO),0)/NVL(SUM(EQUIV_MIN_IND),1))) NRO_CIL_IND, 
               VENTAS_GLP_SEGMENTO('LOJAG',TO_CHAR(SYSDATE,'YYYY/MM/DD'),X.CODI_INVE_CLIE,'IND',SUM(CODI_TARI_IND),20) REAL_IND,
               TO_CHAR(SYSDATE,'DD-MM-YYYY HH24:MI:SS') FECHAREPORTE 
        FROM (SELECT B.CODI_INVE_CLIE,B.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD, SUM(B.CUPO_CANT_PROD) CUPO, SUM(B.CUPO_CANT_PROD) CANTIDAD, 
              SUM(NUME_INVE_PROD_EQU0) EQUIV_MIN_IND, SUM(FACT_CONV_PROD_EQU0) CODI_TARI_IND
              FROM INVE_METAV_GLP_DAT A, INVE_METAV_GLP_DET_DAT B, INVE_CILINDROS_DAT C WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
              AND A.CODI_ADMI_EMPR_FINA = B.CODI_ADMI_EMPR_FINA AND B.CODI_ADMI_EMPR_FINA = C.CODI_ADMI_EMPR_FINA 
              AND A.INVE_CODI_META = B.INVE_CODI_META AND B.CODI_INVE_PROD = C.CODI_INVE_PROD 
              AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY/MM/DD') 
              AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY/MM/DD') AND C.CODI_INVE_TIPO_PRES = 'GLP' 
              AND C.CODI_INVE_TIPO_PROD = 'DOM' 
              AND B.CUPO_CANT_PROD >= 0
              GROUP BY B.CODI_INVE_CLIE,B.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD                                                                                                                         
              UNION ALL                                                                                                                         
              SELECT D.CODI_INVE_CLIE,M.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD, SUM(0) CUPO,SUM(M.UNID_INVE_MOVI)*-1 TOTAL_REAL,0 EQUIV_MIN_IND,0 CODI_TARI_IND 
              FROM INVE_DOCUMENTOS_DAT D, INVE_MOVIMIENTOS_DAT M, INVE_CILINDROS_DAT C WHERE D.CODI_ADMI_EMPR_FINA = 'LOJAG' 
              AND D.CODI_ADMI_EMPR_FINA = M.CODI_ADMI_EMPR_FINA AND M.CODI_ADMI_EMPR_FINA = C.CODI_ADMI_EMPR_FINA AND D.CODI_INVE_TIPO_DOCU = M.CODI_INVE_TIPO_DOCU 
              AND D.CODI_INVE_DOCU = M.CODI_INVE_DOCU AND M.CODI_INVE_PROD = C.CODI_INVE_PROD AND C.CODI_INVE_TIPO_PRES = 'GLP' AND C.CODI_INVE_TIPO_PROD = 'DOM' 
              AND D.CODI_INVE_TIPO_DOCU IN (SELECT T.CODI_INVE_TIPO_DOCU FROM INVE_TIPOS_DOCUMENTOS_REF T WHERE T.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                            AND T.ASOC_INVE_TIPO_DOCU = 'C' AND T.CLAS_INVE_TIPO_DOCU = 6 AND T.ASIE_CONT_INVE_TIPO_DOCU = 1) 
              AND D.CODI_ADMI_ESTA IN ('P','O') 
              AND D.FECH_INVE_DOCU >= (SELECT A.INVE_FECH_INI FROM INVE_METAV_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                       AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY/MM/DD') 
                                       AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY/MM/DD')) 
              AND D.FECH_INVE_DOCU <= (SELECT A.INVE_FECH_FIN FROM INVE_METAV_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                       AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY/MM/DD') 
                                       AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY/MM/DD')) 
              AND D.CODI_INVE_CLIE NOT IN (SELECT CD2.CODI_INVE_CLIE FROM INVE_CUPOS_GLP_DET_DAT CD2 WHERE CD2.INVE_CODI_CUPO = '%(periodo_id)s' 
                                           AND CD2.CODI_INVE_CLIE = D.CODI_INVE_CLIE AND CD2.CODI_INVE_TARI = M.CODI_INVE_TARI)
              GROUP BY D.CODI_INVE_CLIE,M.CODI_INVE_TARI, C.CODI_INVE_TIPO_PROD                                                                                                 
              UNION ALL                                                                                                 
              SELECT T.CODI_INVE_CLIE,T.CODI_INVE_TARI_DOM15,'DOM' CODI_INVE_TIPO_PROD, SUM(0) CUPO, SUM(T.CARG_DOM_15KG) * -1 TOTAL_PEDIDO15,0 EQUIV_MIN_IND,0 CODI_TARI_IND
              FROM TBL_TURNOS_DISTRIBUIDOR T WHERE T.CODI_ADMI_EMPR_FINA = 'LOJAG' AND T.ESTADO_TURNO = 'E' 
              AND T.FECHA_ENTRD >= (SELECT A.INVE_FECH_INI FROM INVE_METAV_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                    AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY/MM/DD') 
                                    AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY/MM/DD')) 
              AND T.FECHA_ENTRD <= (SELECT A.INVE_FECH_FIN FROM INVE_METAV_GLP_DAT A WHERE A.CODI_ADMI_EMPR_FINA = 'LOJAG' 
                                    AND A.INVE_FECH_INI <= TO_DATE('%(fecha_inicio)s','YYYY/MM/DD') 
                                    AND A.INVE_FECH_FIN >= TO_DATE('%(fecha_fin)s','YYYY/MM/DD')) 
              AND T.CODI_INVE_CLIE NOT IN (SELECT CD2.CODI_INVE_CLIE FROM INVE_CUPOS_GLP_DET_DAT CD2 WHERE CD2.INVE_CODI_CUPO = '%(periodo_id)s'  
                                           AND CD2.CODI_INVE_CLIE = T.CODI_INVE_CLIE AND CD2.CODI_INVE_TARI = T.CODI_INVE_TARI_DOM15)                        
              GROUP BY T.CODI_INVE_CLIE,T.CODI_INVE_TARI_DOM15                 
        ) X, INVE_CLIENTES_DAT C, INVE_TARIFAS_DAT T                 
        WHERE X.CODI_INVE_CLIE = C.CODI_INVE_CLIE 
        AND X.CODI_INVE_TARI = T.CODI_INVE_TARI 
        GROUP BY X.CODI_INVE_CLIE,C.NOMB_INVE_CLIE,X.CODI_INVE_TARI,T.NOMB_INVE_TARI,CODI_INVE_TIPO_PROD 
        ORDER BY C.NOMB_INVE_CLIE        
        """ % parametros
        cursor.execute(sql)
        rows = dictfetchall(cursor)
        for rw in rows:
            real_ind = int(rw['REAL_IND'])
            min_ind = int(rw['NRO_CIL_IND'])
            rw['SALDO_IND'] = real_ind - min_ind

        data = {"fecha": rows[0]['FECHAREPORTE'], "registros": rows}
    finally:
        cursor.close()

    return data
