from django.db import models


class Vehiculo(models.Model):
    id_vehi = models.IntegerField(primary_key=True)
    codi_admi_empr_fina = models.CharField(max_length=5, blank=True, null=True)
    placa_vehi = models.CharField(max_length=10, blank=True, null=True)
    nomb_vehi = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_vehi_lojag'

