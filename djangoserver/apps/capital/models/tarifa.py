from django.db import models


class Tarifa(models.Model):
    codi_admi_empr_fina = models.CharField(max_length=5)
    codi_inve_tari = models.CharField(primary_key=True, max_length=5)
    nomb_inve_tari = models.CharField(max_length=80, blank=True, null=True)
    memo_inve_tari = models.CharField(max_length=20, blank=True, null=True)
    codi_admi_ubic_geog = models.CharField(max_length=24, blank=True, null=True)
    defa_inve_tari = models.CharField(max_length=1, blank=True, null=True)
    esta_tran_inve_tari = models.CharField(max_length=1, blank=True, null=True)
    segm_inve_tari = models.CharField(max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inve_tarifas_dat'
        unique_together = (('codi_admi_empr_fina', 'codi_inve_tari'),)
        verbose_name = 'tarifa'
        verbose_name_plural = 'tarifas'
        ordering = ['nomb_inve_tari']

    def __str__(self):
        return "%s - %s" % (self.codi_inve_tari, self.nomb_inve_tari)