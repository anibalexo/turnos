from django.db import models
from .tarifa import Tarifa
from .documento import Documento


class Turno(models.Model):
    id_turno = models.IntegerField(primary_key=True)
    codi_admi_empr_fina = models.CharField(max_length=5, default='LOJAG')
    num_turno_pape = models.IntegerField(blank=True, null=True)
    tipo_clie = models.CharField(max_length=5, blank=True, null=True)
    codi_inve_clie = models.IntegerField()
    nomb_inve_clie = models.CharField(max_length=60, blank=True, null=True)
    codi_inve_trsp = models.IntegerField(blank=True, null=True)
    chof_inve_trsp = models.CharField(max_length=60, blank=True, null=True)
    iden_inve_trsp = models.CharField(max_length=15, blank=True, null=True)
    marc_inve_trsp = models.CharField(max_length=50, blank=True, null=True)
    plac_inve_trsp = models.CharField(max_length=10, blank=True, null=True)
    turno_ingr = models.IntegerField()
    fecha_entrd = models.DateField(blank=True, null=True, auto_now_add=True)
    hora_entrd = models.CharField(max_length=10, blank=True, null=True)
    fecha_sald = models.DateField(blank=True, null=True, auto_now_add=True)
    hora_sald = models.CharField(max_length=10, blank=True, null=True)
    cil_llen_15kg = models.IntegerField(blank=True, null=True)
    num_fact_llen15kg = models.CharField(max_length=20, blank=True, null=True, default='0')
    cil_vaci_15kg = models.IntegerField(blank=True, null=True)
    num_fact_vac15kg = models.CharField(max_length=20, blank=True, null=True, default='0')
    cil_fuga_15kg = models.IntegerField(blank=True, null=True)
    num_fact_fug15kg = models.CharField(max_length=20, blank=True, null=True, default='0')
    tot_cil_15kg = models.IntegerField(blank=True, null=True)
    cil_llen_45kg = models.IntegerField(blank=True, null=True)
    num_fact_llen45kg = models.CharField(max_length=20, blank=True, null=True, default='0')
    cil_vaci_45kg = models.IntegerField(blank=True, null=True)
    num_fact_vac45kg = models.CharField(max_length=20, blank=True, null=True, default='0')
    cil_fuga_45kg = models.IntegerField(blank=True, null=True)
    num_fact_fug45kg = models.CharField(max_length=20, blank=True, null=True, default='0')
    tot_cil_45kg = models.IntegerField(blank=True, null=True)
    cil_llen_otro = models.IntegerField(blank=True, null=True)
    num_fact_llen_otrokg = models.CharField(max_length=20, blank=True, null=True, default='0')
    cil_vaci_otro = models.IntegerField(blank=True, null=True)
    num_fact_vaci_otrokg = models.CharField(max_length=20, blank=True, null=True, default='0')
    cil_fuga_otro = models.IntegerField(blank=True, null=True)
    num_fact_fuga_otrokg = models.CharField(max_length=20, blank=True, null=True, default='0')
    tot_cil_otro = models.IntegerField(blank=True, null=True)
    tot_cil_ingr = models.IntegerField(blank=True, null=True)
    carg_dom_15kg = models.IntegerField(blank=True, null=True)
    num_fact_dom15kg = models.CharField(max_length=20, blank=True, null=True, default='0')
    carg_ind_15kg = models.IntegerField(blank=True, null=True)
    num_fact_ind15kg = models.CharField(max_length=20, blank=True, null=True, default='0')
    carg_agr_15kg = models.IntegerField(blank=True, null=True)
    num_fact_agr15kg = models.CharField(max_length=20, blank=True, null=True, default='0')
    tot_carg_15kg = models.IntegerField(blank=True, null=True)
    carg_ind_45kg = models.IntegerField(blank=True, null=True)
    num_fact_ind45kg = models.CharField(max_length=20, blank=True, null=True, default='0')
    carg_agr_45kg = models.IntegerField(blank=True, null=True)
    num_fact_agri45kg = models.CharField(max_length=20, blank=True, null=True, default='0')
    tot_carg_45kg = models.IntegerField(blank=True, null=True)
    carg_otros_ind = models.IntegerField(blank=True, null=True)
    num_fact_ind_otrokg = models.CharField(max_length=20, blank=True, null=True, default='0')
    carg_otros_agr = models.IntegerField(blank=True, null=True)
    num_fact_agr_otrokg = models.CharField(max_length=20, blank=True, null=True, default='0')
    tot_carg_otro = models.IntegerField(blank=True, null=True)
    estado_turno = models.CharField(max_length=20, default='E')
    estado_desgloce = models.CharField(max_length=2, default='N')
    estado_verificado = models.CharField(max_length=2, default='N')
    tot_es_cil_15kg = models.IntegerField(blank=True, null=True, default=0)
    tot_es_cil_45kg = models.IntegerField(blank=True, null=True, default=0)
    tot_es_cil_otrokg = models.IntegerField(blank=True, null=True, default=0)
    tot_es_cil = models.IntegerField(blank=True, null=True, default=0)
    es_carg_dom15kg = models.IntegerField(blank=True, null=True, default=0)
    es_carg_ind15kg = models.IntegerField(blank=True, null=True, default=0)
    es_carg_agr15kg = models.IntegerField(blank=True, null=True, default=0)
    tot_es_carg_15kg = models.IntegerField(blank=True, null=True, default=0)
    es_carg_ind45kg = models.IntegerField(blank=True, null=True, default=0)
    es_carg_agr45kg = models.IntegerField(blank=True, null=True, default=0)
    tot_es_carg_45kg = models.IntegerField(blank=True, null=True, default=0)
    es_carg_ind_otrokg = models.IntegerField(blank=True, null=True, default=0)
    es_carg_agr_otrokg = models.IntegerField(blank=True, null=True, default=0)
    tot_es_carg_otrokg = models.IntegerField(blank=True, null=True, default=0)
    devol_cil_15kg = models.IntegerField(blank=True, null=True, default=0)
    devol_cil_45kg = models.IntegerField(blank=True, null=True, default=0)
    prest_cil_15kg = models.IntegerField(blank=True, null=True, default=0)
    prest_cil_45kg = models.IntegerField(blank=True, null=True, default=0)
    sucu_crea = models.CharField(max_length=15, blank=True, null=True)
    inve_transf_prod = models.CharField(max_length=2, default='N')
    inve_transf_prod_d45 = models.CharField(max_length=2, default='N')
    inve_transf_prod_i15 = models.CharField(max_length=2, default='N')
    inve_transf_prod_i45 = models.CharField(max_length=2, default='N')
    inve_transf_prod_a15 = models.CharField(max_length=2, default='N')
    inve_transf_prod_a45 = models.CharField(max_length=2, default='N')
    inve_transf_prod_o15 = models.CharField(max_length=2, default='N')
    inve_transf_prod_o45 = models.CharField(max_length=2, default='N')
    codi_inve_tari_dom15 = models.ForeignKey(Tarifa, models.DO_NOTHING, db_column='codi_inve_tari_dom15', to_field='codi_inve_tari', related_name="turnos")
    #codi_inve_tari_ind = models.CharField(max_length=5, blank=True, null=True)
    codi_inve_tari_ind = models.ForeignKey(Tarifa, models.DO_NOTHING, db_column='codi_inve_tari_ind', to_field='codi_inve_tari', related_name="turnosind")

    class Meta:
        managed = False
        db_table = 'tbl_turnos_distribuidor'
        verbose_name = 'turno'
        verbose_name_plural = 'turnos'
        ordering = ['-fecha_entrd', '-turno_ingr']

    def __str__(self):
        return str(self.id_turno)

    def get_guias(self):
        """
        Obtiene las guias vinculadas al turno
        :return: array guias
        """
        return Documento.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', codi_inve_tipo_docu="GCLI", orde_comp_inve_docu=self.id_turno)

