from django.db import models


class Comprobante(models.Model):
    id = models.IntegerField(primary_key=True)
    ref_id = models.CharField(max_length=15, blank=True, null=True)
    numero = models.CharField(max_length=17, blank=True, null=True)
    clave = models.CharField(max_length=49, blank=True, null=True)
    numaut = models.CharField(max_length=49, blank=True, null=True)
    fechaaut = models.DateField(blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True)
    tipo = models.CharField(max_length=1, blank=True, null=True)
    error = models.CharField(max_length=1024, blank=True, null=True)
    xml = models.BinaryField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ecomprob'
