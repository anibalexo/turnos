from django.db import models


class InveCxcPendientes(models.Model):
    codi_admi_empr_fina = models.CharField(max_length=5)
    codi_inve_clie = models.IntegerField()
    codi_inve_tipo_clie = models.CharField(max_length=5)
    codi_inve_tipo_docu = models.CharField(max_length=5)
    codi_inve_docu = models.IntegerField(primary_key=True)
    fech_inve_docu = models.DateField()
    impo_pend_inve_docu = models.DecimalField(max_digits=17, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inve_cxc_pendientes'

    def __str__(self):
        return str(self.codi_inve_clie)
