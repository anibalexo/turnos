from django.db import models


class ClienteTransportista(models.Model):
    codi_admi_empr_fina = models.CharField(primary_key=True, max_length=5)
    codi_inve_clie = models.ForeignKey('Cliente', models.DO_NOTHING, db_column='codi_inve_clie', related_name='transportistas')
    codi_inve_trsp = models.ForeignKey('Transportista', models.DO_NOTHING, db_column='codi_inve_trsp', related_name='clientes')
    defa_inve_trsp = models.BigIntegerField(blank=True, null=True)
    esta_tran_clie_tran = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inve_cliente_transportista_dat'
        unique_together = (('codi_inve_clie', 'codi_inve_trsp'),)