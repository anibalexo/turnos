from django.db import models


class Periodo(models.Model):
    codi_admi_empr_fina = models.CharField(max_length=5)
    inve_codi_cupo = models.IntegerField(primary_key=True)
    inve_codi_desc = models.CharField(max_length=24)
    inve_fech_ini = models.DateField(blank=True, null=True)
    inve_fech_fin = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inve_cupos_glp_dat'
        verbose_name = 'periodo'
        verbose_name_plural = 'periodos'
        ordering = ['-inve_codi_cupo']

    def __str__(self):
        return "%s - %s" % (self.inve_codi_cupo, self.inve_codi_desc)
