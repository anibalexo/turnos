from django.db import models


class Establecimiento(models.Model):
    codi_admi_empr_fina = models.CharField(max_length=5)
    codi_sucu_admi_sucu = models.CharField(max_length=15)
    codi_inve_esta = models.CharField(primary_key=True, max_length=3)
    codi_inve_punt = models.CharField(max_length=3)
    dire_inve_esta = models.CharField(max_length=120)

    class Meta:
        managed = False
        db_table = 'inve_establecimientos_dat'
