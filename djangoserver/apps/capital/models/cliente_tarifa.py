from django.db import models
#from ..models.tarifa import Tarifa
#from ..models.cliente import Cliente

class ClienteTarifa(models.Model):
    codi_inve_clie = models.ForeignKey('Cliente', models.DO_NOTHING, db_column='codi_inve_clie', related_name='tarifas')
    codi_admi_empr_fina = models.CharField(primary_key=True, max_length=5)
    codi_inve_tari = models.ForeignKey('Tarifa', models.DO_NOTHING, db_column='codi_inve_tari', related_name='tarifas')
    defa_inve_tari = models.CharField(max_length=1, blank=True, null=True)
    esta_tran_clie_tari = models.CharField(max_length=1, blank=True, null=True)
    dire_inve_dest = models.CharField(max_length=80, blank=True, null=True)
    codi_inve_stc = models.CharField(max_length=20, blank=True, null=True)
    cont_cupos_vent = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inve_clientes_tarifas_dat'
        unique_together = (('codi_admi_empr_fina', 'codi_inve_clie', 'codi_inve_tari'),)



class ClienteTarifaTbl(models.Model):
    codi_admi_empr_fina = models.CharField(max_length=5, blank=True, null=True)
    #codi_inve_tari = models.CharField(max_length=5, blank=True, null=True)
    codi_inve_tari = models.CharField(primary_key=True, max_length=5)
    desc_inve_tari = models.CharField(max_length=120, blank=True, null=True)
    defa_inve_tari = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_clientes_tarifas'
