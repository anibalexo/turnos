from .tarifa import Tarifa
from .turno import Turno
from .cliente import Cliente
from .periodo import Periodo
from .cliente_tarifa import ClienteTarifa, ClienteTarifaTbl
from .transportista import Transportista
from .cupo import Cupo
from .documento import Documento
from .producto import Producto
from .comprobante import Comprobante
from .movimiento import Movimiento
from .establecimiento import Establecimiento
from .vehiculos_lojagas import Vehiculo
from .tipo_documento import TipoDocumento
from .inve_cxc_pendientes import InveCxcPendientes
from .meta import Meta
