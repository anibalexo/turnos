from django.db import models


class Meta(models.Model):
    codi_admi_empr_fina = models.CharField(max_length=5)
    #inve_codi_meta = models.IntegerField()
    inve_codi_meta = models.ForeignKey('PeriodoMeta', models.DO_NOTHING, db_column='inve_codi_meta', related_name='metas')
    inve_codi_det = models.IntegerField(primary_key=True)
    codi_inve_clie = models.IntegerField()
    codi_inve_tari = models.CharField(max_length=5)
    codi_inve_prod = models.CharField(max_length=24)
    codi_cupo_defi = models.CharField(max_length=1)
    cupo_cant_prod = models.IntegerField(blank=True, null=True)
    saldo_cant_prod = models.IntegerField(blank=True, null=True)
    tipo_cupo_dat = models.CharField(max_length=1, blank=True, null=True)
    sucu_crea = models.CharField(max_length=15)
    user_crea = models.CharField(max_length=24, blank=True, null=True)
    fech_crea = models.DateField(blank=True, null=True)
    user_actu = models.CharField(max_length=24, blank=True, null=True)
    fech_actu = models.DateField(blank=True, null=True)
    codi_inve_prod_equ0 = models.CharField(max_length=24)
    fact_conv_prod_equ0 = models.IntegerField()
    nume_inve_prod_equ0 = models.IntegerField()
    codi_inve_prod_equ1 = models.CharField(max_length=24)
    fact_conv_prod_equ1 = models.IntegerField()
    nume_inve_prod_equ1 = models.IntegerField()
    codi_inve_prod_equ2 = models.CharField(max_length=24)
    fact_conv_prod_equ2 = models.IntegerField()
    nume_inve_prod_equ2 = models.IntegerField()
    codi_inve_prod_equ3 = models.CharField(max_length=24)
    fact_conv_prod_equ3 = models.IntegerField()
    nume_inve_prod_equ3 = models.IntegerField()
    codi_inve_prod_equ4 = models.CharField(max_length=24)
    fact_conv_prod_equ4 = models.IntegerField()
    nume_inve_prod_equ4 = models.IntegerField()
    codi_inve_prod_equ5 = models.CharField(max_length=24)
    fact_conv_prod_equ5 = models.IntegerField()
    nume_inve_prod_equ5 = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'inve_metav_glp_det_dat'
        unique_together = (('inve_codi_meta', 'inve_codi_det', 'codi_inve_clie', 'codi_inve_tari'),)
