from django.db import models


class Transportista(models.Model):
    codi_admi_empr_fina = models.CharField(max_length=5)
    codi_inve_trsp = models.CharField(primary_key=True, max_length=5)
    desc_inve_trsp = models.CharField(max_length=50)
    codi_inve_tras = models.CharField(max_length=5)
    dire_inve_trsp = models.CharField(max_length=50, blank=True, null=True)
    telf_inve_trsp = models.CharField(max_length=20, blank=True, null=True)
    mail_inve_trsp = models.CharField(max_length=80, blank=True, null=True)
    marc_inve_trsp = models.CharField(max_length=30, blank=True, null=True)
    plac_inve_trsp = models.CharField(max_length=10, blank=True, null=True)
    chof_inve_trsp = models.CharField(max_length=80, blank=True, null=True)
    iden_inve_trsp = models.CharField(max_length=15, blank=True, null=True)
    codi_inve_esta = models.CharField(max_length=1)
    user_crea = models.CharField(max_length=30)
    fecha_crea = models.DateField()
    user_actual = models.CharField(max_length=30, blank=True, null=True)
    fecha_actual = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inve_transportistas_dat'
        unique_together = (('codi_admi_empr_fina', 'codi_inve_trsp'),)
        verbose_name = 'transportista'
        verbose_name_plural = 'transportistas'
        ordering = ['-codi_inve_trsp']

    def __str__(self):
        return "%s" % (self.codi_inve_trsp)