from django.contrib import admin

from ..capital.models.cliente import Cliente
from ..capital.models.periodo import Periodo
from ..capital.models.periodo_meta import PeriodoMeta
from ..capital.models.tarifa import Tarifa
from ..capital.models.transportista import Transportista
from ..capital.models.turno import Turno


class TurnoModelAdmin(admin.ModelAdmin):
    """
    Model admin para Turno
    """
    list_display = ('id_turno', 'turno_ingr', 'tipo_clie', 'codi_inve_clie',
                    'nomb_inve_clie', 'fecha_entrd', 'sucu_crea')
    search_fields = ('id_turno', 'nomb_inve_clie')
    list_per_page = 25

admin.site.register(Turno, TurnoModelAdmin)

class TarifaModelAdmin(admin.ModelAdmin):
    """
    Model admin para Tarifa
    """
    list_display = ('codi_inve_tari', 'nomb_inve_tari', 'esta_tran_inve_tari', 'segm_inve_tari')
    search_fields = ('codi_inve_tari', 'nomb_inve_tari')
    list_per_page = 25

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(Tarifa, TarifaModelAdmin)

class ClienteModelAdmin(admin.ModelAdmin):
    """
    Model admin para Cliente
    """
    list_display = ('codi_inve_clie', 'codi_inve_tipo_clie', 'codi_admi_esta', 'iden_inve_clie',
                    'nomb_inve_clie', 'deno_come_inve_clie')
    search_fields = ('codi_inve_clie', 'nomb_inve_clie')
    list_per_page = 25

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(Cliente, ClienteModelAdmin)


class PeriodoModelAdmin(admin.ModelAdmin):
    """
    Model admin para Periodo
    """
    list_display = ('inve_codi_cupo', 'inve_codi_desc', 'inve_fech_ini', 'inve_fech_fin')
    search_fields = ('inve_codi_cupo', 'inve_codi_desc')
    list_per_page = 25

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(Periodo, PeriodoModelAdmin)

class PeriodoMetaModelAdmin(admin.ModelAdmin):
    """
    Model admin para Periodo
    """
    list_display = ('inve_codi_meta', 'inve_codi_desc', 'inve_fech_ini', 'inve_fech_fin')
    search_fields = ('inve_codi_meta', 'inve_codi_desc')
    list_per_page = 25

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(PeriodoMeta, PeriodoMetaModelAdmin)

class TransportistaModelAdmin(admin.ModelAdmin):
    """
    Model admin para Transportista
    """
    list_display = ('codi_inve_trsp', 'desc_inve_trsp', 'tipo_transportista', 'marc_inve_trsp', 'plac_inve_trsp')
    search_fields = ('desc_inve_trsp', 'plac_inve_trsp')
    list_per_page = 25

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def tipo_transportista(self, obj):
        if obj.codi_inve_tras == '1':
            return "LOJA CATAMAYO"
        elif obj.codi_inve_tras == '2':
            return "DISTRIBUIDOR"
        elif obj.codi_inve_tras == '3':
            return "PETROCOMERCIAL"
        elif obj.codi_inve_tras == '4':
            return "AUTOTANQUES"
        elif obj.codi_inve_tras == '5':
            return "BLOQUEADOS"

    tipo_transportista.empty_value_display = '-'

admin.site.register(Transportista, TransportistaModelAdmin)