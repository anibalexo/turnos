from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .serializers import TarifaSerializer
from ...capital.models import Tarifa, Cliente


class TarifaViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API para Tarifa

    list:
        Consulta lista de tarifas

    retrieve:
        Consulta detalle de una tarifa
    """
    queryset = Tarifa.objects.using('capital').filter(codi_admi_empr_fina='LOJAG')
    permission_classes = (IsAuthenticated,)
    serializer_class = TarifaSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['segm_inve_tari']

    def list(self, request, *args, **kwargs):
        if 'cliente_pk' in self.kwargs:
            cliente = Cliente.objects.using('capital').get(pk=self.kwargs['cliente_pk'])
            cliente_tarifas = cliente.tarifas.filter(codi_admi_empr_fina='LOJAG').order_by('-defa_inve_tari').values_list('codi_inve_tari', flat=True)
            queryset = self.get_queryset().filter(pk__in=cliente_tarifas)
            serializer = TarifaSerializer(self.filter_queryset(queryset), many=True, context={"request": request, "cliente": cliente.pk})
        else:
            queryset = self.get_queryset()
            serializer = TarifaSerializer(self.filter_queryset(queryset), many=True, context={"request": request})
        return Response(serializer.data)