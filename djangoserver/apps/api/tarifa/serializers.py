from rest_framework import serializers

from ...capital.models import Tarifa, ClienteTarifa


class TarifaSerializer(serializers.ModelSerializer):
    """
    API para operaciones con Tarifa
    """
    default_tarifa = serializers.SerializerMethodField()

    class Meta:
        model = Tarifa
        fields = (
            'pk',
            'nomb_inve_tari',
            'esta_tran_inve_tari',
            'segm_inve_tari',
            'default_tarifa'
        )

    def get_default_tarifa(self, obj):
        """
        Verifica si la tarifa esta
        asignada por default
        """
        cliente_tarifa = ClienteTarifa.objects.get(codi_inve_clie=self.context['cliente'], codi_inve_tari=obj.pk)
        result = False
        if cliente_tarifa.defa_inve_tari == '1':
            result = True
        return result
