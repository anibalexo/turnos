
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend

from ...capital.models import Turno
from .serializers import TurnoListSerializer, TurnoCreateUpdateSerializer, TurnoRetrieveSerializer


class TurnoViewSet(viewsets.ModelViewSet):
    """
    API para turnos

    list:
        Consulta todos los turnos

    retrieve:
        Consulta detalle de un turno

    create:
        Crear un turno

    update:
        Actualizar atributos de un turno

    delete:
        Eliminar un turno
    """
    queryset = Turno.objects.using('capital').filter(codi_admi_empr_fina='LOJAG')
    permission_classes = (IsAuthenticated,)
    serializer_class = TurnoCreateUpdateSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['tipo_clie', 'fecha_entrd',]

    def get_queryset(self):
        """ Solo turnos por la sucursal vinculada al usuario """
        queryset = Turno.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', sucu_crea=self.request.user.sucursal.codigo)
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = TurnoListSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None, *args, **kwargs):
        turno = get_object_or_404(self.queryset, pk=pk)
        serializer = TurnoRetrieveSerializer(turno)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = TurnoCreateUpdateSerializer(data=request.data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, pk=None):
        queryset = get_object_or_404(self.get_queryset(), pk=pk)
        serializer = TurnoCreateUpdateSerializer(queryset, data=request.data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
