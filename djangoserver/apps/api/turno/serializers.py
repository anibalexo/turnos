from datetime import datetime
from rest_framework import serializers

from ...capital.models import Turno
from ...usuarios.models import Sucursal


class TurnoListSerializer(serializers.ModelSerializer):
    """
    Serializer para listar turnos
    """
    class Meta:
        model = Turno
        fields = ('id_turno', 'estado_turno', 'nomb_inve_clie',)


class TurnoRetrieveSerializer(serializers.ModelSerializer):
    """
    Serializer para detalle de turno
    """
    nombre_sucursal = serializers.SerializerMethodField()
    nombre_tarifa_dom = serializers.CharField(source="codi_inve_tari_dom15.nomb_inve_tari")
    nombre_tarifa_ind = serializers.CharField(source="codi_inve_tari_ind.nomb_inve_tari")
    guias = serializers.SerializerMethodField()

    class Meta:
        model = Turno
        fields = (
            '__all__'
        )
        extra_fields = ('nombre_sucursal', 'nombre_tarifa_dom', 'nombre_tarifa_ind', 'guias',)

    def get_nombre_sucursal(self, obj):
        """
        Nombre de sucursal
        :param obj: instancia
        :return: string nombre sucursal
        """
        try:
            sucursal = Sucursal.objects.get(codigo=obj.sucu_crea)
            return sucursal.nombre
        except Exception as e:
            print(e)
            return None

    def get_guias(self, obj):
        """
        Retorna las guias vinculadas al turno
        :param obj:
        :return:
        """
        lista_guias = []
        for item in obj.get_guias():
            lista_guias.append({
                "codigo_documento": item.codi_inve_docu,
                #"fecha": item.fech_inve_docu,
                #"chofer": item.codi_inve_trsp.chof_inve_trsp,
                "guia": item.nume_fact_inve_rete
            })
        return lista_guias


class TurnoCreateUpdateSerializer(serializers.ModelSerializer):
    """
    Serializer para crear y actualizar turnos
    """
    class Meta:
        model = Turno
        fields = (
            'id_turno',
            'tipo_clie',
            'codi_inve_clie',
            'nomb_inve_clie',
            'codi_inve_trsp',
            'chof_inve_trsp',
            'iden_inve_trsp',
            'marc_inve_trsp',
            'plac_inve_trsp',
            'turno_ingr',
            'cil_llen_15kg',
            'cil_vaci_15kg',
            'cil_fuga_15kg',
            'tot_cil_15kg',
            'cil_llen_45kg',
            'cil_vaci_45kg',
            'cil_fuga_45kg',
            'tot_cil_45kg',
            'cil_llen_otro',
            'cil_vaci_otro',
            'cil_fuga_otro',
            'tot_cil_otro',
            'tot_cil_ingr',
            'carg_dom_15kg',
            'carg_ind_15kg',
            'carg_agr_15kg',
            'tot_carg_15kg',
            'carg_ind_45kg',
            'carg_agr_45kg',
            'tot_carg_45kg',
            'carg_otros_ind',
            'carg_otros_agr',
            'tot_carg_otro',
            'estado_turno',
            'codi_inve_tari_dom15',
            'codi_inve_tari_ind',
            )

    def create(self, validated_data):
        """
        Crear un nuevo turno
        :param validated_data: data de instancia
        :return: instancia
        """
        user = self.context["request"].user
        turno = Turno(**validated_data)
        turno.sucu_crea = user.sucursal.codigo
        turno.num_turno_pape = turno.id_turno
        now = datetime.now()
        turno.hora_entrd = now.strftime("%H:%M:%S")
        turno.hora_sald = now.strftime("%H:%M:%S")
        turno.save()
        return turno

    def update(self, instance, validated_data):
        """
        Actualiza campos de modelo turno
        :param instance: instancia de turno
        :param validated_data: objeto turno con datos
        :return: instancia de turno
        """
        instance.tot_es_cil_15kg = validated_data.get('tot_es_cil_15kg', instance.tot_cil_15kg)
        instance.tot_es_cil_45kg = validated_data.get('tot_es_cil_45kg', instance.tot_cil_45kg)
        instance.tot_es_cil_otrokg = validated_data.get('tot_es_cil_otrokg', instance.tot_cil_otro)
        instance.tot_es_cil = validated_data.get('tot_es_cil', instance.tot_cil_ingr)
        instance.tot_es_carg_15kg = validated_data.get('tot_es_carg_15kg', instance.tot_carg_15kg)
        instance.tot_es_carg_45kg = validated_data.get('tot_es_carg_45kg', instance.tot_carg_45kg)
        instance.tot_es_carg_otrokg = validated_data.get('tot_es_carg_otrokg', instance.tot_carg_otro)
        now = datetime.now()
        instance.hora_sald = now.strftime("%H:%M:%S")
        instance.estado_turno = validated_data.get('estado_turno', instance.estado_turno)
        if instance.estado_turno == "S":
            instance.es_carg_dom15kg = validated_data.get('es_carg_dom15kg', instance.carg_dom_15kg)
            instance.es_carg_ind15kg = validated_data.get('es_carg_ind15kg', instance.carg_ind_15kg)
            instance.es_carg_agr15kg = validated_data.get('es_carg_agr15kg', instance.carg_agr_15kg)
            instance.es_carg_ind45kg = validated_data.get('es_carg_ind45kg', instance.carg_ind_45kg)
            instance.es_carg_agr45kg = validated_data.get('es_carg_agr45kg', instance.carg_agr_45kg)
            instance.es_carg_ind_otrokg = validated_data.get('es_carg_ind_otrokg', instance.carg_otros_ind)
            instance.es_carg_agr_otrokg = validated_data.get('es_carg_agr_otrokg', instance.carg_otros_agr)

        instance.save()
        return instance
