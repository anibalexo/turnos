from django.conf import settings
from django.http import HttpResponse
from django.template.loader import render_to_string
from rest_framework import views, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from weasyprint import HTML, CSS

from ...capital.models import Periodo
from ...capital.utils.querys_capital import (
    get_reporte_stock_disponible_cupo,
    get_reporte_stock_disponible_meta,
    get_reporte_stock_minmax_cupo,
    get_reporte_stock_minmax_meta)


def get_total_despachos(lista):
    """Calcula el total de despachos de una lista"""
    total = 0
    for registro in lista:
        total += registro["DESPACHOS"]
    return total


def get_total_control(lista):
    """Calcula el total de cupos establecidos de una lista"""
    total = 0
    for registro in lista:
        total += registro["CUPO"]
    return total


def filtrar_sin_cupo(lista):
    """Filtra registros con el cupo mayor a 0"""
    result = []
    for registro in lista:
        if registro["CUPO"] != 0:
            result.append(registro)
    return result


def get_total_saldo_global(lista):
    """Calcula total saldo mayor a 0 de una lista"""
    total = 0
    for registro in lista:
        if registro["SALDO_REAL"] >= 0:
            total += registro["SALDO_REAL"]
    return total


def get_total_despacho_extra(lista):
    """Calcula total saldo menor a 0 de una lista"""
    total = 0
    for registro in lista:
        if registro["SALDO_REAL"] < 0:
            total += registro["SALDO_REAL"]
    return total


def get_total_min_nodespachado(lista):
    """Calcula total minimo despachado"""
    total = 0
    for registro in lista:
        if registro["SALDO_IND"] < 0:
            total += registro["SALDO_IND"]
    return total


def get_total_excendentes(lista):
    """Calcula total excedentes"""
    total = 0
    for registro in lista:
        if registro["SALDO_IND"] > 0:
            total += registro["SALDO_IND"]
    return total


class ReporteStockDisponibleViewSet(views.APIView):
    """
    API para consultar stock disponible y
    despachos para distribuidor
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        periodo_id = self.request.query_params.get('periodo', None)
        if periodo_id is None:
            return Response({'error': 'No se encontro el id del periodo'}, status.HTTP_400_BAD_REQUEST)

        periodo = Periodo.objects.using('capital').get(pk=periodo_id)
        tipo_control = self.request.query_params.get('tipo', None)
        if tipo_control is None:
            return Response({'error': 'No se encontro el tipo de control'}, status.HTTP_400_BAD_REQUEST)
        result = {}
        if tipo_control == "cupo":
            result = get_reporte_stock_disponible_cupo(periodo)
        elif tipo_control == "meta":
            result = get_reporte_stock_disponible_meta(periodo)
        return Response(result, status.HTTP_200_OK)


class ReporteStockMinimosMaximosViewSet(views.APIView):
    """
    API para consultar stock disponible y
    despachos con minimos y maximo para distribuidor
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        periodo_id = self.request.query_params.get('periodo', None)
        if periodo_id is None:
            return Response({'error': 'No se encontro el id del periodo'}, status.HTTP_400_BAD_REQUEST)
        periodo = Periodo.objects.using('capital').get(pk=periodo_id)
        tipo_control = self.request.query_params.get('tipo', None)
        if tipo_control is None:
            return Response({'error': 'No se encontro el tipo de control'}, status.HTTP_400_BAD_REQUEST)
        result = {}
        if tipo_control == 'cupo':
            result = get_reporte_stock_minmax_cupo(periodo)
        elif tipo_control == 'meta':
            result = get_reporte_stock_minmax_meta(periodo)
        return Response(result, status.HTTP_200_OK)


class ReporteStockDisponiblePDFViewSet(views.APIView):
    """
    API para consultar stock disponible para distribuidor PDF
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        periodo_id = self.request.query_params.get('periodo', None)
        if periodo_id is None:
            return Response({'error': 'No se encontro el id del periodo'}, status.HTTP_400_BAD_REQUEST)

        periodo = Periodo.objects.using('capital').get(pk=periodo_id)
        tipo_control = self.request.query_params.get('tipo', None)
        if tipo_control is None:
            return Response({'error': 'No se encontro el tipo de control'}, status.HTTP_400_BAD_REQUEST)
        registros = []
        data = []
        if tipo_control == "cupo":
            registros = get_reporte_stock_disponible_cupo(periodo)
            data = filtrar_sin_cupo(registros["registros"])
        elif tipo_control == "meta":
            registros = get_reporte_stock_disponible_meta(periodo)
            data = registros["registros"]
        nombre_report = "REPORTE STOCK DISPONIBLE POR DISTRIBUIDOR"
        html_template = render_to_string('stock_disponible.html', {"nombre_report": nombre_report, "tipo_control": tipo_control,
                                                                   "periodo": periodo, "data": data, "fecha": registros["fecha"],
                                                                   "total_control": get_total_control(data),
                                                                   "total_despachos": get_total_despachos(data),
                                                                   "total_global": get_total_saldo_global(data),
                                                                   "total_despacho_extra": get_total_despacho_extra(data)
                                                                   })
        pdf_file = HTML(string=html_template, base_url=request.build_absolute_uri()).write_pdf(
            stylesheets=[CSS(settings.STATIC + '/css/weasyprint/report.css'), CSS(settings.STATIC + '/css/bootstrap.min.css')])
        response = HttpResponse(pdf_file, content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="reporte.pdf"'
        return response


class ReporteStockDespachadoPDFViewSet(views.APIView):
    """
    API para consultar stock despachado para distribuidor PDF
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        periodo_id = self.request.query_params.get('periodo', None)
        if periodo_id is None:
            return Response({'error': 'No se encontro el id del periodo'}, status.HTTP_400_BAD_REQUEST)

        periodo = Periodo.objects.using('capital').get(pk=periodo_id)
        tipo_control = self.request.query_params.get('tipo', None)
        if tipo_control is None:
            return Response({'error': 'No se encontro el tipo de control'}, status.HTTP_400_BAD_REQUEST)
        registros = []
        data = []
        if tipo_control == "cupo":
            registros = get_reporte_stock_disponible_cupo(periodo)
            data = filtrar_sin_cupo(registros["registros"])
        elif tipo_control == "meta":
            registros = get_reporte_stock_disponible_meta(periodo)
            data = registros["registros"]
        nombre_report = "REPORTE STOCK DESPACHADO POR DISTRIBUIDOR"
        html_template = render_to_string('stock_despachado.html', {"nombre_report": nombre_report, "tipo_control": tipo_control,
                                                                   "periodo": periodo, "data": data, "fecha": registros["fecha"],
                                                                   "total_control": get_total_control(data),
                                                                   "total_despachos": get_total_despachos(data),
                                                                   "total_global": get_total_saldo_global(data),
                                                                   "total_despacho_extra": get_total_despacho_extra(data)})
        pdf_file = HTML(string=html_template, base_url=request.build_absolute_uri()).write_pdf(
            stylesheets=[CSS(settings.STATIC + '/css/weasyprint/report.css'), CSS(settings.STATIC + '/css/bootstrap.min.css')])
        response = HttpResponse(pdf_file, content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="reporte.pdf"'
        return response


class ReporteStockMinimosMaximosPDFViewSet(views.APIView):
    """
    API para consultar stock disponible y
    despachos con minimos y maximo para distribuidor PDF
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        periodo_id = self.request.query_params.get('periodo', None)
        if periodo_id is None:
            return Response({'error': 'No se encontro el id del periodo'}, status.HTTP_400_BAD_REQUEST)
        periodo = Periodo.objects.using('capital').get(pk=periodo_id)
        tipo_control = self.request.query_params.get('tipo', None)
        if tipo_control is None:
            return Response({'error': 'No se encontro el tipo de control'}, status.HTTP_400_BAD_REQUEST)
        registros = []
        data = []
        if tipo_control == "cupo":
            registros = get_reporte_stock_minmax_cupo(periodo)
            data = filtrar_sin_cupo(registros["registros"])
        elif tipo_control == "meta":
            registros = get_reporte_stock_minmax_meta(periodo)
            data = registros["registros"]
        nombre_report = "REPORTE STOCK MINIMOS Y MAXIMOS POR DISTRIBUIDOR"
        html_template = render_to_string('stock_minmax.html', {"nombre_report": nombre_report, "tipo_control": tipo_control,
                                                               "periodo": periodo, "data": data, "fecha": registros["fecha"],
                                                               "total_control": get_total_control(data),
                                                               "total_despachos": get_total_despachos(data),
                                                               "total_min_nodespachado": get_total_min_nodespachado(data),
                                                               "total_excendentes": get_total_excendentes(data)})
        pdf_file = HTML(string=html_template, base_url=request.build_absolute_uri()).write_pdf(
            stylesheets=[CSS(settings.STATIC + '/css/weasyprint/report.css'), CSS(settings.STATIC + '/css/bootstrap.min.css')])
        response = HttpResponse(pdf_file, content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="reporte.pdf"'
        return response
