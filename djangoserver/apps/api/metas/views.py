from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import views
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .serializers import MetaListSerializer
from ...capital.models import Meta
from ...capital.utils.querys_capital import actualiza_meta_distribuidor


class MetaViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API para metas

    list:
        Consulta de todas las metas

    retrieve:
        Consulta detalle de una meta
    """
    queryset = Meta.objects.using('capital').filter(codi_admi_empr_fina='LOJAG')
    permission_classes = (IsAuthenticated,)
    serializer_class = MetaListSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['inve_codi_meta', 'codi_inve_clie',]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        activos = self.request.query_params.get('activos', None)
        if activos is not None:
            queryset = self.filter_queryset(self.get_queryset().filter(cupo_cant_prod__gt=0))
        serializer = MetaListSerializer(queryset, many=True)
        return Response(serializer.data)


class ActualizarMetaClienteViewSet(views.APIView):
    """
    Actualizar meta de un cliente
    """
    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):
        pk_meta = int(request.data['pk'])
        pk_periodo = int(request.data['inve_codi_meta'])
        pk_cliente = int(request.data['codi_inve_clie'])
        pk_tarifa = str(request.data['codi_inve_tari'])
        cantidad = str(request.data['cupo_cant_prod'])
        min_domestico = str(request.data['nume_inve_prod_equ0'])
        result = actualiza_meta_distribuidor(pk_meta, pk_periodo, pk_cliente, pk_tarifa, cantidad, min_domestico)
        if result is False:
            return Response({'error': 'No se pudo actualizar la meta'}, status.HTTP_400_BAD_REQUEST)
        return Response(result, status.HTTP_200_OK)
