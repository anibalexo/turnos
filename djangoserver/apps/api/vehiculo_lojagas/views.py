from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .serializers import VehiculoSerializer
from ...capital.models.vehiculos_lojagas import Vehiculo


class VehiculoViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API para Cliente

    list:
        Consulta de todos los vehiculos

    retrieve:
        Consulta detalle de un vehiculo
    """
    queryset = Vehiculo.objects.using('capital').all()
    permission_classes = (IsAuthenticated,)
    serializer_class = VehiculoSerializer
