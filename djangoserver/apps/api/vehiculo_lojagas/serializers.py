from rest_framework import serializers

from ...capital.models import Vehiculo


class VehiculoSerializer(serializers.ModelSerializer):
    """
    API para Vehiculos de Lojagas
    """
    class Meta:
        model = Vehiculo
        fields = (
            'pk',
            'placa_vehi',
            'nomb_vehi'
        )
