import datetime

from rest_framework import views, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ...capital.models import ClienteTarifa, Periodo, Turno, TipoDocumento, InveCxcPendientes
from ...capital.utils.querys_capital import get_saldo_cliente, get_turnos_pendientes, actualiza_saldos_cliente


class ActualizarSaldosViewSet(views.APIView):
    """
    Actualiza saldos de facturas pendientes
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        result = actualiza_saldos_cliente()
        return Response(result, status.HTTP_200_OK)


class FacturasPendientesViewSet(views.APIView):
    """
    Consulta facturas pendientes de pagar a cliente distribuidor
    #
    El parametro a enviar es `cliente_id`
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        cliente_id = self.request.query_params.get('cliente_id', None)
        if cliente_id is None:
            return Response({'error': 'No se encontro el id del cliente'}, status.HTTP_400_BAD_REQUEST)

        tipo_docs = TipoDocumento.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', asoc_inve_tipo_docu='C',
                                                                  clas_inve_tipo_docu__in=[4, 9, 10]).values_list('codi_inve_tipo_docu', flat=True)
        docs = InveCxcPendientes.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', impo_pend_inve_docu__gt=0, codi_inve_tipo_docu__in=tipo_docs,
                                                                 codi_inve_clie=cliente_id).exclude(codi_inve_tipo_docu__in=['NDCLI', 'NDINC'])
        result = {'existe': False, 'docs': []}
        if len(docs) > 0:
            result['existe'] = True
            result['docs'] = len(docs)

        return Response(result, status.HTTP_200_OK)


class SaldoClienteViewSet(views.APIView):
    """
    Consulta de saldos para cliente con tipo cupo o meta

    #
    Los parametros a enviar son `cliente_id`, `tarifa_dom_id`, `tarifa_ind_id`
    """
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        today = datetime.date.today()
        fecha = today.strftime("%Y/%m/%d")
        empresa = 'LOJAG'
        sucursal = 0
        producto_dom = 'P-CILI-D15'
        producto_ind = 'P-CILI-N15'

        cliente_id = self.request.query_params.get('cliente_id', None)
        if cliente_id is None:
            return Response({'error': 'No se encontro el id del cliente'}, status.HTTP_400_BAD_REQUEST)

        tarifa_dom = self.request.query_params.get('tarifa_dom_id', None)
        tarifa_ind = self.request.query_params.get('tarifa_ind_id', None)
        result = {}
        if tarifa_dom is not None:
            aplica_cupo_dom = self.verifica_aplica_cupo(cliente_id, tarifa_dom, today)
            saldo_dom = 0
            cargas_dom15_cf = 0
            cargas_dom15_sf = 0
            if aplica_cupo_dom:
                saldo_cli = get_saldo_cliente(empresa, fecha, cliente_id, producto_dom, tarifa_dom, sucursal)
                total_pendientes = get_turnos_pendientes(empresa, fecha, cliente_id, producto_dom, tarifa_dom, sucursal)
                saldo_dom = saldo_cli - abs(total_pendientes)
                cargas = self.verifica_cargas_pendientes(cliente_id, tarifa_ind)
                cargas_dom15_cf = cargas['facturadas']
                cargas_dom15_sf = cargas['sin_facturar']

            result["aplica_cupo_dom"] = aplica_cupo_dom
            result['saldo_dom'] = saldo_dom
            result['cargas_dom15_cf'] = cargas_dom15_cf
            result['cargas_dom15_sf'] = cargas_dom15_sf

        if tarifa_ind is not None:
            aplica_cupo_ind = self.verifica_aplica_cupo(cliente_id, tarifa_ind, today)
            saldo_ind = 0
            if aplica_cupo_ind:
                saldo_ind = get_saldo_cliente(empresa, fecha, cliente_id, producto_ind, tarifa_ind, sucursal)

            result["aplica_cupo_ind"] = aplica_cupo_ind
            result['saldo_ind'] = saldo_ind

        return Response(result, status.HTTP_200_OK)

    def verifica_cargas_pendientes(self, id_cliente, id_tarifa):
        """
        Verifica si existen turnos pendientes
        de cerrar con factura y sin factura
        :param id_cliente: pk de cliente
        :param id_tarifa: pk de tarifa
        :return: { facturadas, sin_facturar}
        """
        facturadas = 0
        sin_facturar = 0
        turnos = Turno.objects.using('capital').filter(estado_turno='E', codi_inve_clie=id_cliente,
                                                       codi_inve_tari_dom15=id_tarifa)
        for item in turnos:
            if item.num_fact_dom15kg == '0':
                sin_facturar += item.carg_dom_15kg
            else:
                facturadas += item.carg_dom_15kg

        return {'facturadas': facturadas, 'sin_facturar': sin_facturar}

    def verifica_aplica_cupo(self, id_cliente, id_tarifa, fecha_actual):
        """
        Verifica si la tarifa del cliente
        es controlada por cupos
        :param id_cliente:
        :param id_tarifa:
        :param fecha_actual:
        :return: boolean
        """
        result = False
        if self.verifica_tabla_cupos(fecha_actual, id_cliente, id_tarifa):
            try:
                cliente = ClienteTarifa.objects.using('capital').get(codi_inve_clie=id_cliente, codi_inve_tari=id_tarifa)
                if cliente.cont_cupos_vent == 'S':
                    result = True
            except ClienteTarifa.DoesNotExist:
                result = False

        return result

    def verifica_tabla_cupos(self, fecha_actual, id_cliente, id_tarifa):
        """
        Verfica si existe en la tabla cupos
        :param fecha_actual:
        :param id_cliente:
        :param id_tarifa:
        :return: boolean
        """
        try:
            periodo_actual = Periodo.objects.using('capital').get(codi_admi_empr_fina='LOJAG', inve_fech_ini__lte=fecha_actual, inve_fech_fin__gte=fecha_actual)
            return periodo_actual.cupos.filter(codi_inve_clie=id_cliente, codi_inve_tari=id_tarifa).exists()
        except Periodo.DoesNotExist:
            return False
