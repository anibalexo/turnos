from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers

from .cliente.views import ClienteViewSet
from .cupos.views import CupoViewSet, ActualizarCupoClienteViewSet
from .documento.views import DocumentoViewSet
from .metas.views import MetaViewSet, ActualizarMetaClienteViewSet
from .reportes.views import (
    ReporteStockDisponibleViewSet,
    ReporteStockMinimosMaximosViewSet,
    ReporteStockDespachadoPDFViewSet,
    ReporteStockMinimosMaximosPDFViewSet,
    ReporteStockDisponiblePDFViewSet
)
from .saldos.views import SaldoClienteViewSet, FacturasPendientesViewSet, ActualizarSaldosViewSet
from .tarifa.views import TarifaViewSet
from .transportista.views import TransportistaViewSet
from .turno.views import TurnoViewSet
from .vehiculo_lojagas.views import VehiculoViewSet
from .placa_capacidad.views import PlacaCapacidadViewSet

router = DefaultRouter()
router.register(r'transportistas', TransportistaViewSet, basename='transportista')
router.register(r'clientes', ClienteViewSet, basename='cliente')
router.register(r'tarifas', TarifaViewSet, basename='tarifa')
router.register(r'turnos', TurnoViewSet, basename='turno')
router.register(r'guias', DocumentoViewSet, basename='guias')
router.register(r'vehiculos', VehiculoViewSet, basename='vehiculos')
router.register(r'cupos', CupoViewSet, basename='cupos')
router.register(r'metas', MetaViewSet, basename='metas')
router.register(r'placas_capacidad',PlacaCapacidadViewSet, basename='placas_capacidad')

cliente = routers.NestedSimpleRouter(router, r'clientes', lookup='cliente')
# clientes/{pk}/transportistas/{pk}
cliente.register(r'transportistas', TransportistaViewSet, basename='cliente_transportistas')
# clientes/{pk}/tarifas/{pk}
cliente.register(r'tarifas', TarifaViewSet, basename='cliente_tarifas')

urlpatterns = [
    path(r'', include((router.urls, 'api'))),
    path(r'', include(cliente.urls)),

    # API REPORTES
    path('cliente/saldos/', SaldoClienteViewSet.as_view(), name="saldo-cliente"),
    path('cliente/pendiente/', FacturasPendientesViewSet.as_view(), name="pendiente-cliente"),
    path('reporte/stock/', ReporteStockDisponibleViewSet.as_view(), name="reporte-stock"),
    path('reporte/minmax/', ReporteStockMinimosMaximosViewSet.as_view(), name="reporte-minmax"),
    path('actualizar/cupo/', ActualizarCupoClienteViewSet.as_view(), name="actualizar-cupo"),
    path('actualizar/meta/', ActualizarMetaClienteViewSet.as_view(), name="actualizar-meta"),

    # reportes pdf
    path('reporte/stock/pdf/', ReporteStockDisponiblePDFViewSet.as_view(), name="reporte-stock-pdf"),
    path('reporte/stock/despachado/pdf/', ReporteStockDespachadoPDFViewSet.as_view(), name="reporte-stock-desp-pdf"),
    path('reporte/minmax/pdf/', ReporteStockMinimosMaximosPDFViewSet.as_view(), name="reporte-minmax-pdf"),

    # actualiza datos
    path('actualiza/saldos/', ActualizarSaldosViewSet.as_view(), name="actualiza-saldos"),

]