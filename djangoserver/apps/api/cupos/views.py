from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import views
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .serializers import CupoListSerializer
from ...capital.models import Cupo
from ...capital.utils.querys_capital import actualiza_cupo_distribuidor


class CupoViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API para consultar cupos

    list:
        Consultar lista de cliente

    retrieve:
        Consultar detalle de un cliente
    """
    queryset = Cupo.objects.using('capital').filter(codi_admi_empr_fina='LOJAG')
    permission_classes = (IsAuthenticated,)
    serializer_class = CupoListSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['inve_codi_cupo', 'codi_inve_clie',]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        activos = self.request.query_params.get('activos', None)
        if activos is not None:
            queryset = self.filter_queryset(self.get_queryset().filter(cupo_cant_prod__gte=0))
        serializer = CupoListSerializer(queryset, many=True)
        return Response(serializer.data)


class ActualizarCupoClienteViewSet(views.APIView):
    """
    Actualizar cupo de cliente
    """
    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):
        pk_cupo = int(request.data['pk'])
        pk_periodo = int(request.data['inve_codi_cupo'])
        pk_cliente = int(request.data['codi_inve_clie'])
        pk_tarifa = str(request.data['codi_inve_tari'])
        cantidad = str(request.data['cupo_cant_prod'])
        min_domestico = str(request.data['nume_inve_prod_equ0'])
        result = actualiza_cupo_distribuidor(pk_cupo, pk_periodo, pk_cliente, pk_tarifa, cantidad, min_domestico)
        if result is False:
            return Response({'error': 'No se pudo actualizar el cupo'}, status.HTTP_400_BAD_REQUEST)
        return Response(result, status.HTTP_200_OK)
