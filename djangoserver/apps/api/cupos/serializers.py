import datetime

from rest_framework import serializers

from ...capital.models import Cliente, Cupo, Periodo, Tarifa


class CupoListSerializer(serializers.ModelSerializer):
    """
    Serializer para listar cupo
    """
    nombre_cliente = serializers.SerializerMethodField()
    nombre_tarifa = serializers.SerializerMethodField()
    editable = serializers.SerializerMethodField()

    class Meta:
        model = Cupo
        fields = (
            'pk',
            'inve_codi_cupo',
            'codi_inve_clie',
            'nombre_cliente',
            'codi_inve_tari',
            'nombre_tarifa',
            'cupo_cant_prod',
            'nume_inve_prod_equ0',
            'editable'
        )

    def get_nombre_cliente(self, obj):
        """
        Retorna nombre de distribuidor
        :param obj: instancia cupo
        :return: json object
        """
        cliente = Cliente.objects.using('capital').get(codi_inve_clie=obj.codi_inve_clie)
        return cliente.nomb_inve_clie

    def get_nombre_tarifa(self, obj):
        """
        Retorna nombre de tarifa
        :param obj: instancia cupo
        :return: json object
        """
        tarifa = Tarifa.objects.using('capital').get(codi_inve_tari=obj.codi_inve_tari)
        return tarifa.nomb_inve_tari

    def get_editable(self, obj):
        """
        Retorna booleano que define si
        esta habilitado editar
        :param obj: instancia cupo
        :return: json object
        """
        result = False
        date_today = datetime.date.today()
        try:
            periodo_actual = Periodo.objects.using('capital').get(codi_admi_empr_fina='LOJAG', inve_fech_ini__lte=date_today, inve_fech_fin__gte=date_today)
            if periodo_actual.inve_codi_cupo == obj.inve_codi_cupo.inve_codi_cupo:
                result = True
        except Periodo.DoesNotExist:
            pass
        return result
