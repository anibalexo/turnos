from rest_framework import serializers
from ...capital.models import Transportista
from ...turnos.models import PlacaCapacidad

class TransportistaSerializer(serializers.ModelSerializer):
    """
    API para operaciones con Trasnportista
    """

    capacidad = serializers.SerializerMethodField()

    class Meta:
        model = Transportista
        fields = (
            'pk',
            'codi_inve_tras',
            'desc_inve_trsp',
            'marc_inve_trsp',
            'iden_inve_trsp',
            'plac_inve_trsp',
            'capacidad',
        )

    def get_capacidad(self, obj):
        """
        Capacidad de carga de vehiculo
        :param obj: instancia
        :return: integer capacidad
        """
        try:
            item = PlacaCapacidad.objects.get(placa=obj.plac_inve_trsp)
            return item.capacidad
        except Exception as e:
            return None
