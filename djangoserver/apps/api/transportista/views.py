from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .serializers import TransportistaSerializer
from ...capital.models import Cliente, Transportista


class TransportistaViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API para Transportista

    list:
        Consulta lista de transportistas

    retrieve:
        Consulta detalle de un transportista
    """
    queryset = Transportista.objects.using('capital').all()
    permission_classes = (IsAuthenticated,)
    serializer_class = TransportistaSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['plac_inve_trsp']

    def list(self, request, *args, **kwargs):
        if 'cliente_pk' in self.kwargs:
            cliente = Cliente.objects.using('capital').get(pk=self.kwargs['cliente_pk'])
            queryset = Transportista.objects.using('capital').filter(
                pk__in=cliente.transportistas.all().values_list('codi_inve_trsp')
            )
        else:
            queryset = self.get_queryset()
        serializer = TransportistaSerializer(queryset, many=True)
        return Response(serializer.data)



