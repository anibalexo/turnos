from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .serializers import DocumentoSerializer
from ...capital.models import Documento


class DocumentoViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API para documentos

    list:
        Consultar todos los documentos (tipo GCLI)

    retrieve:
        Consultar detalle de documento (tipo GCLI)
    """
    queryset = Documento.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', codi_inve_tipo_docu="GCLI")
    permission_classes = (IsAuthenticated,)
    serializer_class = DocumentoSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['fech_inve_docu', 'codi_inve_tipo_docu', 'orde_comp_inve_docu',]

    def get_queryset(self):
        """ Solo documentos de tipo GCLI """
        queryset = Documento.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', codi_inve_tipo_docu="GCLI")
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = DocumentoSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None, *args, **kwargs):
        doc = get_object_or_404(self.get_queryset(), pk=pk)
        serializer = DocumentoSerializer(doc)
        return Response(serializer.data)
