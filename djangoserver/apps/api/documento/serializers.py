from rest_framework import serializers

from ...capital.models import Documento, Transportista, Comprobante, Establecimiento, Movimiento


class DocumentoSerializer(serializers.ModelSerializer):
    """
    API para operaciones con documento
    """
    datos_chofer = serializers.SerializerMethodField()
    datos_cliente = serializers.SerializerMethodField()
    clave_comprobante = serializers.SerializerMethodField()
    movimientos = serializers.SerializerMethodField()
    punto_partida = serializers.SerializerMethodField()
    nume_fact_inve_rete = serializers.SerializerMethodField()

    class Meta:
        model = Documento
        fields = (
            'codi_inve_docu',
            'fech_inve_docu',
            'fech_vcto_inve_docu',
            'nume_fact_inve_rete',
            'datos_chofer',
            'datos_cliente',
            'impo_neto_inve_docu',
            'impo_iva_inve_docu',
            'impo_tota_inve_docu',
            'clave_comprobante',
            'movimientos',
            'punto_partida'
        )

    def get_nume_fact_inve_rete(self, obj):
        """
        Retorna numero de guia con 15 numeros
        :param obj:
        :return:
        """
        resp = obj.nume_fact_inve_rete.split("-")
        sec_guia = resp[2]
        sec_guia_completa = '00' + sec_guia
        return "{}-{}-{}".format(resp[0], resp[1], sec_guia_completa)

    def get_datos_chofer(self, obj):
        """
        Retorna datos de chofer
        :param obj: instancia documento
        :return: json object
        """
        if obj.codi_inve_trsp is not None:
            try:
                transp = Transportista.objects.using('capital').get(codi_inve_trsp=obj.codi_inve_trsp)
                return {
                    "nombre": transp.chof_inve_trsp,
                    "cedula": transp.iden_inve_trsp,
                    "placa": transp.plac_inve_trsp
                }
            except Exception as e:
                print(e)
                return None
        return None

    def get_datos_cliente(self, obj):
        """
        Obtiene datos del cliente
        :param obj: instancia documento
        :return: json object
        """
        return {
            "nombre": obj.codi_inve_clie.nomb_inve_clie,
            "ruc": obj.codi_inve_clie.iden_inve_clie
        }

    def get_clave_comprobante(self, obj):
        """
        Obtiene la clave de acceso
        :param obj: instancia documento
        :return: string
        """
        try:
            comp = Comprobante.objects.using('capital').get(ref_id="{}{}".format(obj.codi_inve_tipo_docu, obj.codi_inve_docu))
            return comp.clave
        except Exception as e:
            print(e)
            return None

    def get_movimientos(self, obj):
        """
        Obtiene movimientos
        :return: json object
        """
        lista = []
        movimientos = Movimiento.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', codi_inve_tipo_docu=obj.codi_inve_tipo_docu, codi_inve_docu=obj.codi_inve_docu)
        for mov in movimientos:
            lista.append({
                "producto": mov.codi_inve_prod.nomb_inve_prod,
                "cantidad": mov.unid_inve_movi,
                "tarifa": mov.codi_inve_tari.nomb_inve_tari
            })
        return lista

    def get_punto_partida(self, obj):
        """
        Obtiene el punto de partida
        :param obj: instancia documento
        :return: string
        """
        if obj.nume_fact_inve_rete is not None:
            try:
                est = Establecimiento.objects.using('capital').get(codi_inve_esta=obj.nume_fact_inve_rete[0:3])
                return est.dire_inve_esta
            except Exception as e:
                print(e)
                return None
        return None
