from rest_framework import serializers

from ...capital.models import Cliente


class ClienteSerializer(serializers.ModelSerializer):
    """
    API para Clientes
    """
    class Meta:
        model = Cliente
        fields = (
            'pk',
            'codi_inve_tipo_clie',
            'iden_inve_clie',
            'nomb_inve_clie'
        )
