from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .serializers import ClienteSerializer
from ...capital.models import Cliente, ClienteTarifa, ClienteTarifaTbl


class ClienteViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API para Cliente

    list:
        Consulta todos los clientes

    retrieve:
        Consulta detalle de un cliente
    """
    queryset = Cliente.objects.using('capital').all()
    permission_classes = (IsAuthenticated,)
    serializer_class = ClienteSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['codi_inve_tipo_clie', 'codi_admi_esta']

    def get_queryset(self):
        """ Solo  clientes activos """
        queryset = Cliente.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', codi_admi_esta='A')
        return queryset

    def list(self, request, *args, **kwargs):
        clientes_tarifas_tbl = ClienteTarifaTbl.objects.all().values_list('codi_inve_tari', flat=True)
        clientes_tarifas = ClienteTarifa.objects.filter(codi_admi_empr_fina='LOJAG', codi_inve_tari__in=clientes_tarifas_tbl).values_list('codi_inve_clie',
                                                                                                                                          flat=True)
        distribuidores = Cliente.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', codi_admi_esta='A', codi_inve_tipo_clie='DIS',
                                                                 codi_inve_clie__in=clientes_tarifas).order_by('nomb_inve_clie')
        cliente_generico = Cliente.objects.using('capital').filter(codi_inve_clie=999)
        queryset = (cliente_generico | distribuidores)
        param_extra = request.query_params.get('extra', None)
        if param_extra == 'otros':
            normales = Cliente.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', codi_admi_esta='A', codi_inve_tipo_clie='EMPL')
            queryset = normales

        serializer = ClienteSerializer(self.filter_queryset(queryset), many=True)
        return Response(serializer.data)
