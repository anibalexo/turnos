from requests import Response
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .serializers import PlacaCapacidadSerializer
from ...turnos.models import PlacaCapacidad


class PlacaCapacidadViewSet(viewsets.ModelViewSet):
    """
    API para placas con capacidad

    list:
        Consulta de todas las placas

    retrieve:
        Consulta detalle de una placa
    """
    queryset = PlacaCapacidad.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = PlacaCapacidadSerializer


