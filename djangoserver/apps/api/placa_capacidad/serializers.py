from rest_framework import serializers

from ...turnos.models import PlacaCapacidad


class PlacaCapacidadSerializer(serializers.ModelSerializer):
    """
    Serializer para listar placas y su capacidad
    """
    class Meta:
        model = PlacaCapacidad
        fields = ('pk', 'placa', 'capacidad', 'fecha_creacion', 'fecha_actualizacion',)