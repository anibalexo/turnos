from django.db import models


class Sucursal(models.Model):
    """
    Representa la ubicacion desde donde se emiten los turnos
    """
    nombre = models.CharField(max_length=60, unique=True)
    codigo = models.IntegerField(default=0, help_text="Ingrese el código de la sucursal, << generado en CAPITAL >>")
    activo = models.BooleanField(default=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'sucursal'
        verbose_name_plural = 'sucursales'
