import uuid

from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models

from ..models.sucursal import Sucursal


class Usuario(AbstractUser):
    """
    Representa al modelo usuario
    con atributos adiccionales
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sucursal = models.ForeignKey(Sucursal, on_delete=models.CASCADE, null=True)
    controla_cupos = models.BooleanField(default=False)
    visualiza_reportes = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.username)

    objects = UserManager()

    class Meta:
        verbose_name = 'usuario'
        verbose_name_plural = 'usuarios'

    def check_is_admin(self):
        """
        Verifica si el usuario pertenece al grupo administrador
        :return: boolean
        """
        result = False
        for group in self.groups.all():
            if group.name == 'ADMINISTRADOR':
                result = True
        return result
