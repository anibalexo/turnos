from django.contrib import admin
from .models import Usuario, Sucursal
from django.contrib.auth.admin import UserAdmin


class UsuarioModelAdmin(UserAdmin):
    """
    Model admin para usuarios
    """
    list_display = ('username', 'first_name', 'last_name', 'controla_cupos', 'visualiza_reportes', 'is_active', 'sucursal')
    readonly_fields = ('date_joined', 'last_login')
    fieldsets = (
        (None, {'fields': ('username', 'password',)}),
        ('Información', {'fields': ('first_name', 'last_name', 'email')}),
        ('Permisos', {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        ('Acciones Aplicación', {'fields': ('controla_cupos', 'visualiza_reportes', 'sucursal')}),
        ('Fechas Importantes', {'fields': ('last_login', 'date_joined')})
    )


class SucursalModelAdmin(admin.ModelAdmin):
    """
    Model admin para sucursal
    """
    list_display = ('nombre','codigo', 'activo')


admin.site.register(Usuario, UsuarioModelAdmin)
admin.site.register(Sucursal, SucursalModelAdmin)

