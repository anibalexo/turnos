from django.db import models


class PlacaCapacidad(models.Model):
    """
    Placa con la capacidad de carga
    """
    placa = models.CharField(max_length=7, unique=True)
    capacidad = models.IntegerField(default=0)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.placa

    class Meta:
        verbose_name = 'placa'
        verbose_name_plural = 'placas'
