from django.urls import path
from django.contrib.auth.decorators import login_required

from ..turnos.views import (
    EntradaDistribuidorTemplateView,
    SalidaDistribuidorTemplateView,
    EntradaEmpleadoTemplateView,
    SalidaEmpleadoTemplateView,
    PlacasCapacidadTemplateView,
    PlacaCapacidadCreateView
)

urlpatterns = [
    path('distribuidor/entrada', login_required(EntradaDistribuidorTemplateView.as_view()), name='distribuidor-entrada'),
    path('distribuidor/salida', login_required(SalidaDistribuidorTemplateView.as_view()), name='distribuidor-salida'),
    path('empleado/pedido', login_required(EntradaEmpleadoTemplateView.as_view()), name='empleado-pedido'),
    path('empleado/salida', login_required(SalidaEmpleadoTemplateView.as_view()), name='empleado-salida'),
    path('placa', login_required(PlacasCapacidadTemplateView.as_view()), name='placa-capacidad'),
    path('placa/nuevo', login_required(PlacaCapacidadCreateView.as_view()), name='placa-capacidad-crear'),
]
