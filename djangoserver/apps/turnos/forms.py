from django import forms
from .models import PlacaCapacidad


class PlacaCapacidadForm(forms.ModelForm):

    class Meta:
        model = PlacaCapacidad
        fields = ['placa', 'capacidad']
        widgets = {
            'placa': forms.TextInput(attrs={'class': 'form-control', 'required': 'True', 'placeholder': 'Ingrese placa 7 digitos'}),
            'capacidad': forms.TextInput(attrs={'class': 'form-control', 'required': 'True'}),
        }