from django.contrib import admin

from .models import PlacaCapacidad


class PlacaCapacidadAdmin(admin.ModelAdmin):
    """
    Model admin para placas y capacidad
    """
    list_display = ('placa','capacidad', 'fecha_creacion', 'fecha_actualizacion',)
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')

admin.site.register(PlacaCapacidad, PlacaCapacidadAdmin)