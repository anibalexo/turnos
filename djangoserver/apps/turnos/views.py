from django.db.models import Max
from django.db.models.functions import Coalesce
from django.utils.datetime_safe import date
from django.views.generic import TemplateView, CreateView
from rest_framework.reverse import reverse_lazy

from .models import PlacaCapacidad
from .forms import PlacaCapacidadForm

from ..capital.models import Cliente, ClienteTarifa, ClienteTarifaTbl, Turno


class EntradaDistribuidorTemplateView(TemplateView):
    template_name = 'turnos/distribuidor_entrada.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        usuario = self.request.user
        fecha_actual = date.today()  # + timedelta(days=31) # agregamos 30 dia a la fecha
        context['secuencias'] = self.get_secuencias(fecha_actual, usuario.sucursal.codigo)
        context['lista_distribuidores'] = self.get_distribuidores()
        return context

    def get_distribuidores(self):
        try:
            clientes_tarifas_tbl = ClienteTarifaTbl.objects.all().values_list('codi_inve_tari', flat=True)
            clientes_tarifas = ClienteTarifa.objects.filter(codi_admi_empr_fina='LOJAG',
                                                            codi_inve_tari__in=clientes_tarifas_tbl
                                                            ).values_list('codi_inve_clie', flat=True)
            data = Cliente.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', codi_inve_tipo_clie='DIS',
                                                           codi_admi_esta='A', codi_inve_clie__in=clientes_tarifas
                                                           ).order_by('nomb_inve_clie')
            return data
        except:
            return None

    def get_secuencias(self, date, sucursal):
        data = Turno.objects.using('capital').filter(fecha_entrd=date, sucu_crea=sucursal, tipo_clie='DIST') \
            .values('num_turno_pape', 'turno_ingr') \
            .aggregate(num_turno_pape=Coalesce(Max('num_turno_pape'), 0) + 1,
                       turno_ingr=Coalesce(Max('turno_ingr'), 0) + 1)

        if data['num_turno_pape'] == 1:  # si es primero del dia, modificamos la query sin el parametro 'fecha_entrd'
            data_pape = Turno.objects.using('capital').filter(sucu_crea=sucursal, tipo_clie='DIST') \
                .values('num_turno_pape') \
                .aggregate(num_turno_pape=Max('num_turno_pape') + 1)
            data['num_turno_pape'] = data_pape['num_turno_pape']
        return data


class SalidaDistribuidorTemplateView(TemplateView):
    template_name = 'turnos/distribuidor_salida.html'


class EntradaEmpleadoTemplateView(TemplateView):
    template_name = 'turnos/empleado_entrada.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        usuario = self.request.user
        fecha_actual = date.today()  # + timedelta(days=31) # agregamos 30 dia a la fecha
        context['secuencias'] = self.get_secuencias(fecha_actual, usuario.sucursal.codigo)
        return context

    def get_secuencias(self, date, sucursal):
        data = {}
        turnos_hoy = Turno.objects.using('capital').filter(fecha_entrd=date, sucu_crea=sucursal)
        if len(turnos_hoy) > 1:
            turno_ingreso = turnos_hoy.filter(tipo_clie='EMPL').values('num_turno_pape', 'turno_ingr') \
                .aggregate(num_turno_pape=Coalesce(Max('num_turno_pape'), 0) + 1, turno_ingr=Coalesce(Max('turno_ingr'), 0) + 1)
            data = turno_ingreso
        else:
            turno_secuencia = Turno.objects.using('capital').filter(sucu_crea=sucursal).values('num_turno_pape')\
                .aggregate(num_turno_pape=Max('num_turno_pape') + 1)
            data = {'num_turno_pape': turno_secuencia['num_turno_pape'], 'turno_ingr': 1}
        return data


class SalidaEmpleadoTemplateView(TemplateView):
    template_name = 'turnos/empleado_salida.html'


class PlacasCapacidadTemplateView(TemplateView):
    template_name = 'turnos/placa_capacidad.html'


class PlacaCapacidadCreateView(CreateView):
    template_name = 'turnos/placa_capacidad_nuevo.html'
    model = PlacaCapacidad
    form_class = PlacaCapacidadForm
    success_url = reverse_lazy('placa-capacidad')

