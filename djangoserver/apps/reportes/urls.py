from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import StockDisponibleTemplateView, StockDespachadoTemplateView, StockMinMaxTemplateView, ConsultaTransportistasTemplateView

urlpatterns = [
    path('stock/disponible', login_required(StockDisponibleTemplateView.as_view()), name='reporte-stock-disponible'),
    path('stock/despachado', login_required(StockDespachadoTemplateView.as_view()), name='reporte-stock-despachado'),
    path('stock/minmax', login_required(StockMinMaxTemplateView.as_view()), name='reporte-stock-minmax'),
    path('consulta/transportistas', login_required(ConsultaTransportistasTemplateView.as_view()), name='reporte-consulta-transportistas'),
]
