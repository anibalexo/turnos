from django.views.generic import TemplateView

from ..capital.models.cliente import Cliente
from ..capital.models.cliente_tarifa import ClienteTarifa, ClienteTarifaTbl
from ..capital.models.periodo import Periodo


class StockDisponibleTemplateView(TemplateView):
    template_name = 'reportes/stock_disponible.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['lista_periodos'] = Periodo.objects.using('capital').all()
        return context


class StockDespachadoTemplateView(TemplateView):
    template_name = 'reportes/stock_despachado.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['lista_periodos'] = Periodo.objects.using('capital').all()
        return context


class StockMinMaxTemplateView(TemplateView):
    template_name = 'reportes/stock_minmax.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['lista_periodos'] = Periodo.objects.using('capital').all()
        return context


class ConsultaTransportistasTemplateView(TemplateView):
    template_name = 'reportes/transportistas_habilitados.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        clientes_tarifas_tbl = ClienteTarifaTbl.objects.all().values_list('codi_inve_tari', flat=True)
        clientes_tarifas = ClienteTarifa.objects.filter(codi_admi_empr_fina='LOJAG',
                                                        codi_inve_tari__in=clientes_tarifas_tbl).values_list('codi_inve_clie', flat=True)
        distribuidores = Cliente.objects.using('capital').filter(codi_admi_empr_fina='LOJAG', codi_admi_esta='A', codi_inve_tipo_clie='DIS',
                                                                 codi_inve_clie__in=clientes_tarifas).order_by('nomb_inve_clie')
        context['lista_distribuidores'] = distribuidores
        return context
