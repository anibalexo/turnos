
"""
Configuracion del Router para verificar
entre multiples bases de datos
"""
db_lojagas = "capital"

class ConfigRouterCapital(object):
    """
    Router de control de lectura y escritura
    de la base de datos capital para la app capital.
    """
    def db_for_read(self, model, **hints):
        """
        Verificación de aplicaciones que
        solo requieren lectura de la base de datos capital
        """
        if model._meta.app_label == "capital":
            return db_lojagas
        return None

    def db_for_write(self, model, **hints):
        """
        Escribir cualquier transacción en capital
        """
        if model._meta.app_label == 'capital':
            return db_lojagas
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Permite la relación si los objetos son de la app capital.
        """
        if obj1._meta.app_label == 'capital' or \
           obj2._meta.app_label == 'capital':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Asegura que la app capital solo aparezca en la base de datos capital
        """
        if app_label == 'capital':
            return db == db_lojagas
        return None
