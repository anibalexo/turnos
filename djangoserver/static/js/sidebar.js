
/*!
 * simplesidebar.js
 */

/* inicializamos tooltip */
$('[data-toggle="tooltip"]').tooltip()

$(document).ready(function(){
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
});


/* funcion reloj digital */
function cargarReloj(){
    var fechahora = new Date();
    var hora = fechahora.getHours();
    var minuto = fechahora.getMinutes();
    var segundo = fechahora.getSeconds();
    if(hora == 0){
        hora = 12;
    }
    hora = (hora < 10) ? "0" + hora : hora;
    minuto = (minuto < 10) ? "0" + minuto : minuto;
    segundo = (segundo < 10) ? "0" + segundo : segundo;
    var tiempo = hora + ":" + minuto + ":" + segundo;
    document.getElementById("relojnumerico").innerText = tiempo;
    document.getElementById("relojnumerico").textContent = tiempo;
    setTimeout(cargarReloj, 500);
}

cargarReloj();