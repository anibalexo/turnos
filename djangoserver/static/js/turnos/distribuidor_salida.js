/* Limpia campos de informacion de orden y desabilita botones */
function limpiarCamposInfo(){
    document.getElementById('nroOrden').value = "-";
    document.getElementById('nroTurno').value = "-";
    document.getElementById('infoEntrada').value = "-";
    document.getElementById('infoDist').value = "-";
    document.getElementById('infoSalida').value = "-";
    document.getElementById('infoTrans').value = "-";
    document.getElementById('infoVehi').value = "-";
    document.getElementById('infoTariDom').value = "-";
    document.getElementById('infoTariInd').value = "-";
    document.getElementById('infoEstado').innerHTML = "";

    document.getElementById("txt15Llenos").value = "0";
    document.getElementById("txt15Vacios").value = "0";
    document.getElementById("txt15Fugas").value = "0";
    document.getElementById("txt15Total").value = "0";

    document.getElementById("txt45Llenos").value = "0";
    document.getElementById("txt45Vacios").value = "0";
    document.getElementById("txt45Fugas").value = "0";
    document.getElementById("txt45Total").value = "0";

    document.getElementById("txtOtrosLlenos").value = "0";
    document.getElementById("txtOtrosVacios").value = "0";
    document.getElementById("txtOtrosFugas").value = "0";
    document.getElementById("txtOtrosTotal").value = "0";

    document.getElementById("txtTotal").value = "0";

    document.getElementById("txtFactDom15").innerText = "0";
    document.getElementById("txtNumFactDom15").value = "0";
    document.getElementById("txtFactInd15").innerText = "0";
    document.getElementById("txtNumFactInd15").value = "0";
    document.getElementById("txtFactAgr15").innerText = "0";
    document.getElementById("txtNumFactAgr15").value = "0";
    document.getElementById("txtTotalFact15").innerText = "0";

    document.getElementById("txtFactInd45").innerText = "0";
    document.getElementById("txtNumFactInd45").value = "0";
    document.getElementById("txtFactAgr45").innerText = "0";
    document.getElementById("txtNumFactAgr45").value = "0";
    document.getElementById("txtTotalFact45").innerText = "0";

    document.getElementById("txtFactIndOtro").innerText = "0";
    document.getElementById("txtNumFactIndOtro").value = "0";
    document.getElementById("txtFactAgrOtro").innerText = "0";
    document.getElementById("txtNumFactAgrOtro").value = "0";
    document.getElementById("txtTotalFactOtro").innerText = "0";
    /* Botones de acciones */
    document.getElementById("btnCerrar").disabled = true;
    document.getElementById("btnImprimir").disabled = true;
    document.getElementById("btnAnular").disabled = true;

    document.getElementById("listaGuias").innerHTML = "";
};

/* Verifica si ya tiene factura las cargas a facturar */
function verificarFactura(cantidad, numeroFactura) {
    var resultado = "0";
    if(cantidad > 0){
        resultado = (numeroFactura == "0" || numeroFactura == "GUIA") ? "FACTURA PENDIENTE" : numeroFactura;
    }
    return resultado;
}

/* Obtiene la fecha formateada en yyyy-mm-dd */
function fechaActualFormateada() {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
}

/* Retorna un objeto con atributos de un turno para anular */
function generarJsonDatosTurnoCerrar(){
    var jsonObject = new Object();
    jsonObject.id_turno = document.getElementById("nroOrden").value;
    var dist = document.getElementById("infoDist").value;
    jsonObject.codi_inve_clie = dist.split("-")[0].trim();
    jsonObject.turno_ingr = document.getElementById("nroTurno").value;
    var tariDom = document.getElementById("infoTariDom").value;
    jsonObject.codi_inve_tari_dom15 = tariDom.split("-")[0].trim();
    var tariInd = document.getElementById("infoTariInd").value;
    jsonObject.codi_inve_tari_ind = tariInd.split("-")[0].trim();
    jsonObject.estado_turno = "S";
    return jsonObject;
}

/* Retorna un objeto con atributos de un turno para anular */
function generarJsonDatosTurnoAnular(){
    var jsonObject = new Object();
    jsonObject.id_turno = document.getElementById("nroOrden").value;
    var dist = document.getElementById("infoDist").value;
    jsonObject.codi_inve_clie = dist.split("-")[0].trim();
    jsonObject.turno_ingr = document.getElementById("nroTurno").value;
    var tariDom = document.getElementById("infoTariDom").value;
    jsonObject.codi_inve_tari_dom15 = tariDom.split("-")[0].trim();
    var tariInd = document.getElementById("infoTariInd").value;
    jsonObject.codi_inve_tari_ind = tariInd.split("-")[0].trim();
    jsonObject.estado_turno = "A";
    return jsonObject;
}

/* Actualiza campos de informacion de turno en dom */
function mostrarInfoTurno(data){
    document.getElementById("nroOrden").value = data.id_turno;
    document.getElementById("nroTurno").value = data.turno_ingr;
    document.getElementById("infoEntrada").value = data.fecha_entrd + " " + data.hora_entrd;
    document.getElementById("infoDist").value = data.codi_inve_clie + " - " + data.nomb_inve_clie;
    document.getElementById("infoSalida").value = data.fecha_sald + " " + data.hora_sald;
    document.getElementById("infoTrans").value = data.iden_inve_trsp + " - " + data.chof_inve_trsp;
    document.getElementById("infoVehi").value = data.marc_inve_trsp + " - " + data.plac_inve_trsp;
    document.getElementById("infoTariDom").value = data.codi_inve_tari_dom15 + " - " + data.nombre_tarifa_dom;
    document.getElementById("infoTariInd").value = data.codi_inve_tari_ind + " - " + data.nombre_tarifa_ind;
    var infoestado;
    if (data.estado_turno == "E") {
        infoestado = "<span class='badge badge-primary'>ABIERTA</span>";
    } else if (data.estado_turno == "A") {
        infoestado = "<span class='badge badge-danger'>ANULADA</span>";
    } else if (data.estado_turno == "S") {
        infoestado = "<span class='badge badge-success'>CERRADA</span>";
    }
    document.getElementById('infoEstado').innerHTML = infoestado;

    document.getElementById("txt15Llenos").value = data.cil_llen_15kg;
    document.getElementById("txt15Vacios").value = data.cil_vaci_15kg;
    document.getElementById("txt15Fugas").value = data.cil_fuga_15kg;
    document.getElementById("txt15Total").value = data.tot_cil_15kg;

    document.getElementById("txt45Llenos").value = data.cil_llen_45kg;
    document.getElementById("txt45Vacios").value = data.cil_vaci_45kg;
    document.getElementById("txt45Fugas").value = data.cil_fuga_45kg;
    document.getElementById("txt45Total").value = data.tot_cil_45kg;

    document.getElementById("txtOtrosLlenos").value = data.cil_llen_otro;
    document.getElementById("txtOtrosVacios").value = data.cil_vaci_otro;
    document.getElementById("txtOtrosFugas").value = data.cil_fuga_otro;
    document.getElementById("txtOtrosTotal").value = data.tot_cil_otro;

    document.getElementById("txtTotal").value = data.tot_cil_ingr;

    document.getElementById("txtFactDom15").innerText = data.carg_dom_15kg;
    document.getElementById("txtNumFactDom15").value = verificarFactura(data.carg_dom_15kg, data.num_fact_dom15kg);
    document.getElementById("txtFactInd15").innerText = data.carg_ind_15kg;
    document.getElementById("txtNumFactInd15").value = verificarFactura(data.carg_ind_15kg, data.num_fact_ind15kg);
    document.getElementById("txtFactAgr15").innerText = data.carg_agr_15kg;
    document.getElementById("txtNumFactAgr15").value = verificarFactura(data.carg_agr_15kg, data.num_fact_agr15kg);
    document.getElementById("txtTotalFact15").innerText = data.tot_carg_15kg;

    document.getElementById("txtFactInd45").innerText = data.carg_ind_45kg;
    document.getElementById("txtNumFactInd45").value = verificarFactura(data.carg_ind_45kg, data.num_fact_ind45kg);
    document.getElementById("txtFactAgr45").innerText = data.carg_agr_45kg;
    document.getElementById("txtNumFactAgr45").value = verificarFactura(data.carg_agr_45kg, data.num_fact_agri45kg);
    document.getElementById("txtTotalFact45").innerText = data.tot_carg_45kg;

    document.getElementById("txtFactIndOtro").innerText = data.carg_otros_ind;
    document.getElementById("txtNumFactIndOtro").value = verificarFactura(data.carg_otros_ind, data.num_fact_ind_otrokg);
    document.getElementById("txtFactAgrOtro").innerText = data.carg_otros_agr;
    document.getElementById("txtNumFactAgrOtro").value = verificarFactura(data.carg_otros_agr, data.num_fact_agr_otrokg);
    document.getElementById("txtTotalFactOtro").innerText = data.tot_carg_otro;

    var bodyTable = document.getElementById("listaGuias");
    bodyTable.innerHTML = '';
    data.guias.forEach(function(item) {
        var tr = document.createElement("tr");
        var html = "<td class='small'>" + item.codigo_documento + "</td><td class='small'>" + item.guia +
        "</td><td><button type='button' class='ml-2 badge btn btn-primary btn-sm' data-toggle='modal' data-target='#modalImprimirGuia' onclick='verGuia(" + item.codigo_documento + ")'>IMPRIMIR GUÍA</button></td>";
        tr.innerHTML = html;
        bodyTable.appendChild(tr);
    });
}

/* Construye pdf con datos de guia */
function construirPdfGuia(guia){
    var doc = new jsPDF('p', 'mm', [77.5, 149]);
    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text(13, 7, "COMPROBANTE REPRESENTATIVO");
    doc.text(24, 10, "GUÍA DE REMISIÓN");
    doc.text(15, 17, "1190067927001 - CEM LOJAGAS");
    doc.line(3, 18, 75, 18); //Bycss: Add linea horizontal
    doc.setFontSize(8);
    doc.text(5, 22, "Fecha Ini.:");
    doc.text(20, 22, guia.fech_inve_docu)
    doc.text(39, 22, "Fecha Venc.:");
    doc.text(57, 22, guia.fech_vcto_inve_docu);
    doc.text(34, 27, "Número:");
    doc.setFontSize(16);
    doc.rect(14, 29, 54, 6);
    doc.text(16, 34, guia.nume_fact_inve_rete);
    doc.setFontSize(8);
    doc.text(29, 39, "Clave de acceso:");
    doc.setFontSize(7);
    doc.text(5, 42, guia.clave_comprobante);
    doc.line(3, 44, 75, 44); //Bycss: Add linea horizontal
    doc.setFontSize(8);
    doc.text(5, 48, "Chofer:");
    doc.text(16, 48, guia.datos_chofer.nombre);
    doc.text(5, 52, "Cedula:");
    doc.text(16, 52, guia.datos_chofer.cedula);
    doc.text(43, 52, "Placa:");
    doc.text(52, 52, guia.datos_chofer.placa);
    var partida = doc.splitTextToSize("Partida: " + guia.punto_partida, 69);
    doc.text(5, 57, partida);
    doc.line(3, 66, 75, 66); //Bycss: Add linea horizontal
    var destino = doc.splitTextToSize("Destino: " + guia.movimientos[0].tarifa, 69);
    doc.text(5, 70, destino);
    doc.text(5, 79, "RUC:");
    doc.text(13, 79, guia.datos_cliente.ruc);
    doc.text(5, 83, "Cliente:");
    doc.text(16, 83, guia.datos_cliente.nombre);
    // tabla productos
    doc.rect(5, 87, 53, 5);
    doc.text(6, 91, "Producto");
    doc.rect(58, 87, 15, 5);
    doc.text(59, 91, "Cantidad");
    doc.rect(5, 92, 53, 5);
    doc.text(6, 96, guia.movimientos[0].producto);
    doc.rect(58, 92, 15, 5);
    doc.text(68, 96, guia.movimientos[0].cantidad.toString());
    // tabla valores
    doc.text(35, 108, "Base Imponible:");
    doc.text(59, 108, guia.impo_neto_inve_docu);
    doc.rect(58, 104, 15, 5);
    doc.text(52, 113, "Iva:");
    doc.text(59, 113, guia.impo_iva_inve_docu);
    doc.rect(58, 109, 15, 5);
    doc.text(49, 118, "Total:");
    doc.text(59, 118, guia.impo_tota_inve_docu);
    doc.rect(58, 114, 15, 5);
    doc.text(5, 125, "Observaciones:");
    return doc.output('datauristring');
}

/* Construye pdf con datos de turno */
function construirPdf(turno){
    var imgData = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/7AARRHVja3kAAQAEAAAAZAAA/9sAQwACAQECAQECAgICAgICAgMFAwMDAwMGBAQDBQcGBwcHBgcHCAkLCQgICggHBwoNCgoLDAwMDAcJDg8NDA4LDAwM/9sAQwECAgIDAwMGAwMGDAgHCAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgARACgAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A/fyiiigAooooAKKKKACiiigAooooAKKKKAGiVTIU3LvUBiueQDnBx+B/KnV4v8SfiVH4V/bL+Ftha3EbQ+LtO1zSbxUkyrSWwtbiLcBxuT98Bnkec3qa9orqxGFlShTm9px5l/4FKP5xZ5+BzGGJq16Md6M+R+rhCa/8lmvncKKKK5T0AooooAKKKKACuP8Ah18fPCHxa8UeItF8O65a6pqnhO4+yatbxq6tZy75E2ncoDfNFIMqSMr16V2FfDv/AAS8/wCTsv2h/wDsNP8A+l99Xt5dllPEYLFYmbadJRats+aSi7/J9LHyeecQVsFm2X4CnFOOJlUjJu90o03NW1tutbp6H3FXF/F79ojwT8BLGO48YeJdL0NZhmKOeTdPMM4ykSguw9wpFVP2ofjnB+zh8CfEPjCaGO5k0u3/ANFt3batxcOwSJCeuC7DOOcA18rfsY/sIQ/tFaWvxc+NE154q1rxWftljYXUjpDHbnOx5FBBIYYKRjCKm3g543yrKcPPDSzDMJuNGL5UopOU5WvaN9FZatvRHHxHxLjqWPp5LklKNTEzi5tzbVOnC9uadvebb0jGOrs3dJHvPgn/AIKPfBbx/rMen2PjqxhupnEcYvraeyV2JwAHmRV5PvXtsUqzxK8bK6OAyspyGB6EGvD/AIq/8E5Pg/8AFHw1LY/8IXo/h+4KMsN7olslhPAx/i/dgK+PSRWHXivG/wBgf4ieJv2cf2ita/Z48aag2p29jC114ZvXyPMjCiXykHJ2PEWcKWIjaKRATxjqqZVl+Lw1SvlUp81Nc0oTs249ZRcbJ26ppO2p5+H4kzvLMfQwXEcKbp15ckKtLmUVUauoThNtpys1GSk02rNK+n2tXH6f8e/CGq/F288B2+uWkni7T7cXVxpgDeZHGQrZzjaTtdDgHOGziuwr4d+G3/Ka3xp/2Bz/AOkdpXnZNllPGRxEqja9nTlNW6tNKz8tT2+KuIK2V1MDCjFS9vXhSle+kZRk21ZrX3VvdeR9r69r1l4X0W61LUrq3sdPsYmnuLieQRxwRqMszMeAABnJrC+E/wAa/Cnx00CbVPCOu2OvWNvMbeWW2YnypAAdrAgEHBB5HINfOf8AwVm+KV7F8L/D/wAMtBZn8Q/ErUorPy0+99mV1yOOfnlMS+hXzK5P4CeG7P8AYN/4KDN8PYXkh8KfEjQrVrCWZjta8gjZeWJPzu6T8Z6zxjGCtejg+G4VcseKlJqrJSlCPRwg0pN9b6tr/Czwsy47q4biCOXQpp4aMqdOrO7vGpVUnTS6W0ipX1XPH5/cFfIf7Xv/AAVe0P4ZfEwfB74Q2MHxU+PmrGS2tNDtXP8AZfh+QD5rnVrpeIIIQS8iKTJhcEJuDV57/wAHCX/BQrWv2Kv2WdM8OeDdRudJ8cfE+eewttRtpDHcaRYQoGu7mJxyk37yGJGBDKZmdSGjFfG37NvwQm/ZN/ZW8F+AvC9is3xY+Pen2uueKNSjYfa0srx82Ojxv1SMriWbnLs53EqQB1cN8L/XIxr1tpN8qeitH4pSf8seys29Lrc6+NONqWS0Kk7OTglot3KTtCEf70n11tG7sz64/Y1nuvi9+2R4K0ux1q68YaL8G9Jv7jU/Eki7F17V74yG+vgB8qpcXUrtHGuAI4vl+UAD9D6+O7j40/A//gih8E/DGi/EDxKljrXjCdpJprawmurvVZkCCaURxqzLbwh0UZwAGGMu5z9RfCX4t+Gfjv8ADfR/GHg7W9P8ReGdftxdWGo2UvmQ3KEkHB7MGBVlOGVlZWAIIHBxNXjiK0auGg1QiuSEmrc1m23ta8pNuy+434Dy/E4PBz/tKaeKrSdWpFO/I5KMYwW7tCEYxvre27PP/wBqH/goD8G/2L9S0my+J3jzSfCd7rkbzWNvPHNNNPGhAZ9kSOyqCQNzAAnIBJBrsPgL+0H4L/ag+GVj4y8A+IrDxR4a1FnSC+tC20ujFXRlYBkZWGCrAEcccivwN/ai0zX/APgtn/wWY8UeHfCuq+Xpen295pGj33kfarex07S4Zm34VkytxelgGLcG8XlgoU++f8Gs/wC1FdeFPiH8Rfgdrkr2w1Bf+Ek0i1nba1tdw7be/hCkdSot2xkYMMhwSxI78ZwrTo5b9YjNutGMZSj0Slfy3Vtdej8jpwfE062YfV5QSpScoxl3lG1/l2/4c/aDVdVtdC0u5vr64t7Oys4mnuLieQRxQRqCzO7HAVQASSeABXx38Qf+DgL9lD4deIJNNm+JratNCSry6NoOoalbKQccTQwNG31ViK+bP+Dqr46+JfBvwS+GXgHS7y4sfDvji+v7vWxCxQ6gtmtv5Ns5B+aIvcGRkI5aKI9AQcP/AII1/wDBMX9kr9qj9i7Q9W1+DTfH3xC1KOT/AISCCTXp7e80OfewFuttDMhjVV2lWZSXB3ZKsBXPgslwkMuWZY9zcZOyULaWurtvTo/6Z04rOMTPMHl+DUbxV25X62sklr1P0n/ZZ/bc+Ff7avhm51b4Y+NNL8UwWJUXkMW+G7sS2dvnW8qrLHnBwWUA7TgnBr1Svxk/Y2/4I4fHz9h//grrZ634FhaL4P6Vqcgk8Q32pwEapoUyFmspYUcSyXCHYuTGqeZEkvA4r9m68nOsFhcNWSwdTnhJJra6v0duv3eh6WU4vE16TeLp8k4tp9nbqvIK+Hf+CXn/ACdl+0P/ANhp/wD0vvq+4q/PX4Z+AP2hv2WP2iPihrfhX4X2/iPT/F2sXMqSXN/bqkkQu55YZUInVgGWUkhgDyMgEYr2OGoxq4HG4XnjGU4w5eaSinaab1bS2R8Hx5Uq4fNspzBUalSnSqVHP2cJTaUqTim1FN2uz3L/AIK2+GbzxJ+xTrjWis39mXtpezgdREsoVj+G8H6A16d+x544074hfsueA9S0to/s39iW1syJ0hlhjWKSPH+y6MPwrxLSPjd+0h491e10PxP8CNDXw3rEy2WrM+qRbVtJCEmJHnnOIyxxg5xjFcMv7Ofxx/4J+eLNUuPg/HH48+H2oTG8fRLsh54CRg5j3I7SBQoEkRJcKu5DgV6EctjUy5ZVWrU41Yyc4Pni4yTSUouSbUZaJq9r7Hj1M9qUM9lxHhsLWnh5040qq9lONSDjJyjOMZJOcLSaly3cbJ+R94E4FfDPj3xDZ/ED/gsnoM2nXKx2fgHQW/ty83qsNqI4bl3LvnAUG7hjYnozEHoabqH7Un7U3x7spNB8M/CebwPNcp5c+q39vNbNbKeC0b3OxQR1+VZGHYZwa9I+Av8AwTT0f4dfs+eLvDfiDUm1bxR8QLRodZ1dQX8knLIsW7llSQ7yzYMjcnA2qsYHBUslhVqY+rHnqRdNRi1NpTspTlZ2SUb2V7tlZxm2J4rq4ehk+Gn7GjUjWlUqRlSjKVO7hThzJSblO3NK1oxT3bPpjSNYtPEGmW99YXVvfWd0glhuLeQSRTIeQyspIYH1HFfEvw24/wCC13jT/sD5/wDJO0qj8HdY/aV/Yr8EyeAYfhnaePdKsZpF0bU7W7HlxB2LYIDBjFuJYK6xsNxG7GMSfCb9lz4yeENH+KXxc13T1vPi54u02Sw0fS7a5hElkZ2RWmLF/LUxoqbEDnCxEEktiunL8rpZesVzYinKFWDpwanG8uaUbNpP3Ukryvaxw51xDis7llyhgq0amHqqvWTpySh7OE7xjJpKbk3aHLfm30PKfHv7a3hG4/4KX3PjzxgdTvvC/gsS6bocFlFHK3mQgxhyGdBtMrzyhgSc+WMEDIP29f8AgoL8P/2k9O8G6l4KTxJYeMvBuq/brG5vLaGONEO1mG5ZWbPmRQkADHynpX19/wAE2/2Urj9lz4Awwa1YxWni7XJmvNUAZJGt1B2wwb1yCEjAJAJAd5MEjFezfE/4d6b8Wvh7rHhrWII7jTdatJLWdXQNtDDAcZ6MpwykchlBGCAa6sRxNlOGzKn7KjKcaCVOMlOycVdN8vK073f2vevvqefgfD/ibMMgxH1jFxpTxjdadOVFuUZyalGLn7ROLjyxXwe5a1nY/Ev/AIOVNVn+Ofg39nb4rWMbN4b8SaNf6czoCyWd8TDK0DHGA5CTADOT9mk7KTXtHwH8X3n7Vnwv+Dvx8+E1jD4m8S/DvS9N0Hxn4XhkzfWN7ZJ5Yfy+pimjB2soPy7GAJD7fdvgx/wTm1b4z/slfEf4CfGbQ7qz8MnUVvPDmtW1zBJLFOd+Lm2wzFCjIsgDqAwndWBBYV+Yvxi/4IgftY/sSfEy41j4e2GueKLe1DR2niXwLqf2S+eEsPkktxKlypOFLIgkTI+82M16mCxuCozeAo1oJ0nJRbd4Tpz15W01rZpOzumuuqOzFZTjc5yynjMfQqRlUUJSSVqlKtSdudKS2um1dNOL9GfU3/BxV418A/HT9k/wX4q8QeG/FXgT4oaXqxs/DVhrUcNvd6pZyhTfgwpK7G3j2RMJZFTD7FX/AFjg+U/8Ex/21PEH7Hf/AARO+P8A4iGoXduyeJRoPgpicCDU720iEjQnrmMsbggZAKMccnPlX7P3/BDj9qH9tP4srq3xA0rXvBtjfyoNV8UeNLv7RqLRrwQlu0jXErhchVfYnI+YCvpz/grb/wAEt/i4fhj8F/gT+z78MNY8SfC/4e2E+pXOpf2pYQyanrNxI0bzXRmniJkC+Y5cIFH2pwMKqqvPOWW06dPKfaxa5+Z6+7FLWybb3elr31b0Wh9JhaWZylUzOVOXNycq0SlOT+01GyVvTolq9T5W/wCCJX/BRL4R/wDBM3xR418Q+OfC/jrXNf1yxtdJ0mXw/aWc8dlZozSTK/2i5hZWdxD93cCIhnoK4TWP25vCvw4/4KyH9oL4X6b4j0XwnJ4tXxHNpuqRQx3zRXWP7ViKxyyxgSmW7KAOQPMU/LgAfuH+zD/wRl+BPwe/Z+8JeG/E3wt8A+K/Eml6bEmr6vf6VHeTX94Runk8yRdxUyFtoOAF2gAAYr5s/wCC0/8AwRE0j4k/BHw/rf7PPwt0DT/GXh/UDHfaVoUdtpp1exlUhsh2jieSKQIwLMDtaQDPArnw/EeU18xm5Rkvarkcm1y22WnTb5XOqvw/mtHL6cYSjJ02ppKL5r7tXvrv21Pp/wD4KZab+zL8f/2dtD0T48+L/D2g+HfEMkep+G9Sm1VLC+hm8vC3Vo7ZPEcuG3K0e2TDqQcV+dnxj/4NZ/iF4QuW1r4R/FfQNbkhTzbSPV45tIuyMZAS5txKpYjODtRSSMlRkif4zf8ABIL4/ftff8E2/hLcav4Ul0X4xfBeG98K/wBgapqNpu8SaGJN9pJFLHI0Uc0SbIgkjgSBJGLJ8gON8EP+CiP/AAUI/Zf+EVh8OYvgP4k8QxeG7YaXp9/q3w41i8vLaGJRGiCa1dILhUVQFfDZAyWcVyZbRxOGocmWYqLalJSjJx5bX0a8mtXZ6+qaOzMalDEV75jhpW5U4yipX807a6P+tUYH/BMz/gqR8ev2Pv26/D/wN+LGr6/4m0LUfE9t4G1DSNauVv73QL2e4S0glt7osXaNZXiyvmPEYmZkGdpP74V+Mv8AwS5/4I3fF/4t/tj2/wC0R+0fZz6LcWus/wDCWWul3rQ/2lrOqF/NhmmhiJW1hhk2SLEdrho412Iqmv2arwuLqmDnio/VuXm5VzuPw83l39fv1PZ4Xp4qGFksTzW5ny83xcvS4UUUV8mfShRRRQAV8G6f+0f8cPjTZfFj4peHfiZ8P/AXhT4X+PbvwrZ+E/E1hFb6XfWenzxwXU2pak2Z7Wacs7R+WoSICLcsu8kfeVeC+Of+CZvwZ+IvxdvPGmq+FrqXUNU1K21rU7CLWr6DRdYv7fb5F3d6ckwtLiZCiHfJExJRSckA16GX4ihTcvbK97W0Uuuqs2rXXXddNzkxVKrO3s3tfq10027dup5H8R/+Ctqaf8dPE3wzsfCbWGpRjxFpmk6wmt2eoSQahpemz3267tIi32eKRIHMQkkMrBQXijDA1zvwl/4LF32n/s9eEfEniTwPrfiTSfDvhPwhffE3xja3FrZ2+jX2tWFncBobMnfcIgu4pZRHt8tJVCiQgqPeIv8AgmJ8HIvG7a8NB1o3S6rqutW9ufEeomxsbrVIbmHUZILbz/Ji+0LdzlwiAFmDcEAiFP8Aglh8EYdT8O3EfhW+ih8N2GjaZHYx65frp+pQ6OqrpgvrYTeVetbBF2NcLIflGScCvRWIyrlUfZy2V/W7v9rS626J62a35PY47Vqavd/d06d/nbqmWv2Vv2z9Z/aa8TeNJJPhtq3hfwX4R1PVdETxJeatazR6neadfy2k6R26HzlTEZcOwA3LIn8Ks/zqn/BY3VtV8TeCvH194L1vwf8ABTVPBfi7xnFNPLZ3t74osdMFmIHVEbfaTZlJELHawuEy+VYL9u/CL4M+HfgX4TuND8M2T2Om3eqahrMsT3Ek5e6vrya8uX3SMxw888rBc4UMAAAAK8d8H/8ABKP4F+CNWlubPwjeTwNpmqaJFp97rt/eabZadqRDXtlBayzNDDbyMN3lxoqqeRjisKGIy9Tm6lN2+zbXSzT3lo72d9UtbJaG1SjirRUZ+vrpbZbb9vVln9hv9vCP9sq68UWM3hG68L6l4XisLqRotRi1XTrqG8SR41ju4gENxGYnSaHGY224Lq6ufk34C/8ABWX4k/Ej4LfsxtqWk+KdP1vxx43g0vxV4nvfDEVv4f1mxK6gZIrWYMVWTEMRBVQcQyHPUH7s/Z+/ZU8I/szx6h/wjP8Awkk82pxW1tNca14hvtanWC3V1ggR7uWRo4oxI+EQgZYk5JzWT4c/YT+GPhP4ZfDfwfYeH5ofD/wl1lNf8MW51G5ZrC8QXAWRnMheUf6TN8shZSWGR8oxUMXl8JztTbi2uXy92Se7f2mna/TyRjUw+MlGFp2aTv56xa2S6Jr5nz0f+C22keH/AIcQ+NPFHwu8YeHfCfirwreeLvAt019Z3Vx4rtbee1gWF4UfNnPK19aMiyMybJss6lWUaPiL/grhqngW88YaH4o+Ek/hXxh4PvtB0+6tNV8Y6bb6Zv1aK8mik+2k8xoloVIjjkmeSQKsRwWrqvGn/BJP4YQ/CXxrofgvTI9F1LxJ4cvfDeltrNxda7pXh21upBNLb2lhcTGK3t3lVGaO38o/Iu0qUXHG/s8f8EcdE8Na34m1/wCImqSal4g1jVNJ1PS5PDes6zZSaBJp1pcWkUsV9NeS30kskd3cK5ebbsZUC4QE9UZ5O4Sm4vyWt3qrfatte+r11VtE8ZRzJTjFNW6vTTR+Xe1tFpvfdXvg5/wVwu/2iPEnw30bwT8H/E2san460S71++hm1mysxoFtZ6w2lXhdpWHmmOVdyKnzSKRwpzj7Mrx/4EfsGfC39mrxRpus+DfDs+l6ho+maho9nI+p3V0IbW+1FtSuY8SyMDuumLhjllB2gheK9grx8fUw0p/7LFxjrvvu7dXsrHpYSFeMP9old+W23outwooorhOoKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD/2Q==';
    var doc = new jsPDF('p', 'mm', [77.5, 149]);
    doc.addImage(imgData, 'JPEG', 05, 03, 35, 17);
    doc.setFontSize(09);
    doc.setFontType("bold");
    doc.text(62, 05, "TURNO");
    doc.rect(59,06,16,11);
    doc.text(12, 24, turno.nombre_sucursal);
    doc.setFontSize(08);
    doc.text(18, 29, "CONTROL DE E/S DE CILINDROS");
    doc.rect(08, 32, 65, 05);
    doc.text(09, 36, "Orden No. : ");
    doc.text(08, 41, "Fecha  :");
    doc.text(47, 41, "Hora:");
    doc.text(08, 45, "Distrib.:");
    doc.text(08, 49, "Chofer :");
    doc.text(08, 53, "Cédula :");
    doc.text(08, 57, "Vehíc.  :");
    doc.text(08, 61, "Placa   :");
    doc.rect(08, 63, 65, 20);//Bycss: add Rect
    doc.line(23, 67, 23, 83);//Bycss: Add Vertical
    doc.line(61, 67, 61, 83);//Bycss: Add Vertical
    doc.line(08, 67, 73, 67);//Bycss: Add linea horizontal
    doc.line(23, 71, 73, 71);//Bycss: Add linea horizontal
    doc.line(23, 75, 73, 75);//Bycss: Add linea horizontal
    doc.line(23, 79, 73, 79);//Bycss: Add linea horizontal
    doc.text(15, 66, "CANTIDAD CILINDROS QUE INGRESAN");
    doc.setFontType("bold");
    doc.setFontSize(07);
    doc.text(26, 70, "Llenos     Vacios     Fugas           Total");
    doc.text(10, 74, "Cil. 15kg");
    doc.text(10, 78, "Cil. 45kg");
    doc.text(10, 82, "Cil. Otro");
    doc.text(42, 86, "Total Cilindros:");
    doc.rect(08, 89, 65, 28);//Bycss: add rect
    doc.line(23, 93, 23, 109);//Bycss: Add V.
    doc.line(61, 93, 61, 109);//Bycss: Add V.
    doc.line(08, 93, 73, 93);//Bycss: Add linea horizontal
    doc.line(23, 97, 73, 97);//Bycss: Add linea horizontal
    doc.line(23, 101, 73, 101);//Bycss: Add linea horizontal
    doc.line(23, 105, 73, 105);//Bycss: Add linea horizontal
    doc.line(08, 109, 73, 109);//Bycss: Add linea horizontal
    doc.setFontType("bold");
    doc.text(11, 92, "SALIDA DE CILINDROS | CARGAS A FACTURAR");
    doc.text(24, 96, "Domést.     Indust.     Agríc.           Total");
    doc.text(09, 100, "Cil. 15kg");
    doc.text(09, 104, "Cil. 45kg");
    doc.text(09, 108, "Cil. Otros");
    doc.text(09, 112, "Id. Destino:");//Bycss: add
    doc.line(08, 129, 35, 129);//Bycss: Add linea horizontal
    doc.line(44, 129, 74, 129);//Bycss: Add linea horizontal
    doc.text(13, 132, "Coord. Plataf.");
    doc.text(50, 132, "Distrib. / Transp.");
    doc.setFontType("italic");
    doc.text(15, 138, "Por favor entregue correctamente sus datos,");
    doc.text(21, 141, "caso contrario perderá su turno");
    doc.setFontSize(27);
    doc.setFontType("normal");
    (turno.turno_ingr < 10) ? doc.text(64, 15, turno.turno_ingr.toString()) : doc.text(62, 15, turno.turno_ingr.toString());
    doc.setFontSize(08);
    doc.setFontType("bold");
    doc.text(35, 36, turno.num_turno_pape.toString());//Id Orden / papeleta
    doc.setFontSize(08);
    doc.setFontType("bold");
    doc.text(57, 41, turno.hora_entrd);
    doc.text(21, 41, turno.fecha_entrd);
    doc.text(21, 45, turno.nomb_inve_clie);
    var chofer = (turno.chof_inve_trsp.length > 25) ? turno.chof_inve_trsp.substr(0,29) + "..." : chofer = turno.chof_inve_trsp;
    doc.text(21, 49, chofer);
    doc.text(21, 53, turno.iden_inve_trsp);
    doc.text(21, 57, turno.marc_inve_trsp);
    doc.text(21, 61, turno.plac_inve_trsp);

    /* Tabla ingreso de cilindros */
    doc.text(28, 74, turno.cil_llen_15kg.toString());
    doc.text(39, 74, turno.cil_vaci_15kg.toString());
    doc.text(50, 74, turno.cil_fuga_15kg.toString());
    doc.text(65, 74, turno.tot_cil_15kg.toString());
    doc.text(28, 78, turno.cil_llen_45kg.toString());
    doc.text(39, 78, turno.cil_vaci_45kg.toString());
    doc.text(50, 78, turno.cil_fuga_45kg.toString());
    doc.text(65, 78, turno.tot_cil_45kg.toString());
    doc.text(28, 82, turno.cil_llen_otro.toString());
    doc.text(39, 82, turno.cil_vaci_otro.toString());
    doc.text(50, 82, turno.cil_fuga_otro.toString());
    doc.text(65, 82, turno.tot_cil_otro.toString());
    doc.text(65, 86, turno.tot_cil_ingr.toString());

    /* Tabla cargas a facturar */
    doc.text(27, 100, turno.carg_dom_15kg.toString());
    doc.text(40, 100, turno.carg_ind_15kg.toString());
    doc.text(50, 100, turno.carg_agr_15kg.toString());
    doc.text(65, 100, turno.tot_carg_15kg.toString());
    doc.text(27, 104, "-");//Bycss: add
    doc.text(40, 104, turno.carg_ind_45kg.toString());
    doc.text(50, 104, turno.carg_agr_45kg.toString());
    doc.text(65, 104, turno.tot_carg_45kg.toString());
    doc.text(27, 108, "-");//Bycss: add
    doc.text(40, 108, turno.carg_otros_ind.toString());
    doc.text(50, 108, turno.carg_otros_agr.toString());
    doc.text(65, 108, turno.tot_carg_otro.toString());

    doc.text(27, 112, turno.codi_inve_tari_dom15);//Bycss: add
    doc.text(38, 112, turno.codi_inve_tari_ind);//Bycss: add
    doc.text(50, 112, "0");//Bycss: add
    doc.text(12, 116, "Destino:");//Bycss: add
    doc.line(08, 113, 73, 113);//Bycss: Add linea horizontal
    var tarif_dome = (turno.nombre_tarifa_dom.length > 14) ? turno.nombre_tarifa_dom.substr(0,24) + "..." : turno.nombre_tarifa_dom;
    doc.text(27, 116, tarif_dome);//Bycss: add
    return doc.output('datauristring');
}
