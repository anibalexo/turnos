
/**
* Genera codigo HTML en string para input capacidad placa
* @param {number} data: campo capacidad en datatables
* @param {array} row: fila de tabla con los campos
* @return {string} html: codigo html
*/
function generarHTMLInputCapacidad(data, row){
    var htmlInput = "<div class='form-inline text-right'><input  class='form-control form-control-sm' id='txt_cap" + row.pk + "' size='8' type='number' value='" + data + "'>";
    return htmlInput
};

/**
* Genera codigo HTML en string para acciones de tabla
* @param {number} data: campo CUPO en datatables
* @param {array} row: fila de tabla con los campos
* @return {string} html: codigo html
*/
function generarHTMLAcciones(data, row){
    var htmlButton = "<div class='form-inline text-right'><button type='button' class='btn btn-outline-secondary btn-sm ml-2' " +
                     "onclick='actualizarCapacidadPlaca(" + row.pk + ", `" + row.placa + "`)'>Actualizar</button>" +
                     "<button type='button' onclick='eliminarCapacidadPlaca(" + row.pk + ",`" + row.placa + "`)' class='btn btn-outline-danger btn-sm ml-2'>Eliminar</button></div>";
    return htmlButton;
};

/**
* Genera objeto json con atributos de cupo
* @param {number} pkCupo: id de registro cupo
* @param {number} periodo: id de periodo cupo
* @param {number} cliente: id de cliente
* @param {number} tarifa: id de tarifa
* @param {number} cupo_nuevo: valor nuevo en cupo
* @return {Object} jsonObject: objecto json
*/
function generarJsonDatosPlacaCapacidad(pkRegistro, placa, capacidad){
    var jsonObject = new Object();
    jsonObject.pk = pkRegistro;
    jsonObject.placa = placa;
    jsonObject.capacidad = capacidad;
    return jsonObject;
};