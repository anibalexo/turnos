
/* Limpia elemento select option */
function limpiarSelect(id) {
    document.getElementById(id).options.length = 0;
};

/* Carga tarifas a elemento select */
function agregarTarifas(listaItems, objSelect){
    listaItems.forEach(function(item) {
        var option = document.createElement("option");
        option.value = item.pk;
        option.text = item.pk + ' - ' + item.nomb_inve_tari;
        if(item.default_tarifa){
            option.selected = "selected";
        }
        objSelect.appendChild(option);
    });
};

/* Cargar valores 0 por defecto a inputs */
function cargarValoresIniciales(lista) {
    lista.forEach(function(item) {
        document.getElementById(item).value = '0';
    });
};

/* Limpia elemento input por id */
function cleanInput(id_element) {
    input = document.getElementById(id_element);
    if(input.value == '0'){
        input.value = "";
    }
};

/* Suma total en base id de inputs ingresados */
function sumarInputs(id_total, ...params) {
    var total = 0;
    params.forEach(function(item) {
        var input = document.getElementById(item);
        if(isNaN(parseInt(input.value))){
            total += 0;
        }else {
            total += parseInt(input.value);
        }
    });
    document.getElementById(id_total).value = total;
    var total15 = document.getElementById('txt15Total').value;
    var total45 = document.getElementById('txt45Total').value;
    var totalOtros = document.getElementById('txtOtroTotal').value;
    document.getElementById('txtTotal').value = parseInt(total15) + parseInt(total45) + parseInt(totalOtros);
};

/* Valida información requerida antes de guardar */
function validarCamposLlenos(){
    var codiDist = document.getElementById('txtCodDist').innerText;
    var nombDist = document.getElementById('txtNomDist').innerText;
    var tariDom = document.getElementById('txtTarDom').innerText;
    var tariInd = document.getElementById('txtTarInd').innerText;
    var codiTran = document.getElementById('txtCodTran').innerText;
    var nombTran = document.getElementById('txtNomTran').innerText;
    var dniTran = document.getElementById('txtDniTran').innerText;
    var totalCilIngresa = document.getElementById('txtTotal').value;
    var totalCilFact15 = document.getElementById('txt15TotalFact').value;
    var totalCilFact45 = document.getElementById('txt45TotalFact').value;
    var totalCilFactOt = document.getElementById('txtOtroTotalFact').value;
    var saldoDomCil = document.getElementById('saldoDom').innerText;
    var totalCilFactDom = document.getElementById('txt15Dom').value;
    var cilVaciosDom15 = document.getElementById('txt15Vacio').value;
    var resultado = false;
    if(codiDist === '-' || nombDist === '-'){
        alert("ATENCIÓN: \n\nNo se ha seleccionado un distribuidor");
    }else if(tariDom === '-' || tariInd === '-'){
        alert("ATENCIÓN: \n\nNo se ha seleccionado una tarifa");
    } else if(codiTran === '-' || nombTran === '-' || dniTran === '-'){
        alert("ATENCIÓN: \n\nNo se ha seleccionado un chofer");
    } else if(totalCilIngresa == 0 ){
        alert("ATENCIÓN: \n\nLa cantidad de cilindros que ingresan con el conductor es 0");
    } else if(totalCilFact15 == 0 && totalCilFact45 == 0 && totalCilFactOt == 0){
        alert("ATENCIÓN: \n\nLa cantidad de cilindros a facturar es 0");
    } else if(parseInt(totalCilFactDom) > parseInt(cilVaciosDom15)){
        alert("ATENCIÓN: \n\n !DATOS INCONSISTENTES¡\n\n La cantidad de cilindros de 15Kg a facturar no puede \nser mayor al número de cilindros vacios ingresados");
    } else if(saldoDomCil !== 'N/A'){
        if(parseInt(totalCilFactDom) > parseInt(saldoDomCil)){
            alert("ATENCIÓN:\n\n !CUPO INSUFICIENTE¡\n\n La cantidad de cilindros de GLP Doméstico a facturar es mayor al saldo disponible: " + saldoDomCil);
        } else {
            resultado = true;
        }
    } else {
        resultado = true;
    }
    return resultado;
};

/* Retorna un objeto con atributos de un turno */
function generarJsonDatosTurnoEmpleado(){
    var jsonObject = new Object();
    jsonObject.id_turno = parseInt(document.getElementById("txtNroOrden").innerText);
    jsonObject.tipo_clie = "EMPL";
    jsonObject.codi_inve_clie = parseInt(document.getElementById("txtCodDist").innerText);
    var nombCli = document.getElementById("txtNomDist").innerText.split("-");
    jsonObject.nomb_inve_clie = nombCli[0].trim();
    jsonObject.codi_inve_trsp = parseInt(document.getElementById("txtCodTran").innerText);
    jsonObject.chof_inve_trsp = document.getElementById("txtNomTran").innerText;
    jsonObject.iden_inve_trsp = document.getElementById("txtDniTran").innerText;
    jsonObject.marc_inve_trsp = document.getElementById("txtMarcVehi").innerText;
    jsonObject.plac_inve_trsp = document.getElementById("txtPlacVehi").innerText;
    jsonObject.turno_ingr = parseInt(document.getElementById("txtNumTurno").innerText);

    jsonObject.cil_llen_15kg = parseInt(document.getElementById("txt15Lleno").value);
    jsonObject.cil_vaci_15kg = parseInt(document.getElementById("txt15Vacio").value);
    jsonObject.cil_fuga_15kg = parseInt(document.getElementById("txt15Fuga").value);
    jsonObject.tot_cil_15kg = parseInt(document.getElementById("txt15Total").value);

    jsonObject.cil_llen_45kg = parseInt(document.getElementById("txt45Lleno").value);
    jsonObject.cil_vaci_45kg = parseInt(document.getElementById("txt45Vacio").value);
    jsonObject.cil_fuga_45kg = parseInt(document.getElementById("txt45Fuga").value);
    jsonObject.tot_cil_45kg = parseInt(document.getElementById("txt45Total").value);

    jsonObject.cil_llen_otro = parseInt(document.getElementById("txtOtroLleno").value);
    jsonObject.cil_vaci_otro = parseInt(document.getElementById("txtOtroVacio").value);
    jsonObject.cil_fuga_otro = parseInt(document.getElementById("txtOtroFuga").value);
    jsonObject.tot_cil_otro = parseInt(document.getElementById("txtOtroTotal").value);
    jsonObject.tot_cil_ingr = parseInt(document.getElementById("txtTotal").value);

    jsonObject.carg_dom_15kg = parseInt(document.getElementById("txt15Dom").value);
    jsonObject.carg_ind_15kg = parseInt(document.getElementById("txt15Ind").value);
    jsonObject.carg_agr_15kg = parseInt(document.getElementById("txt15Agr").value);
    jsonObject.tot_carg_15kg = parseInt(document.getElementById("txt15TotalFact").value);

    jsonObject.carg_ind_45kg = parseInt(document.getElementById("txt45Ind").value);
    jsonObject.carg_agr_45kg = parseInt(document.getElementById("txt45Agr").value);
    jsonObject.tot_carg_45kg = parseInt(document.getElementById("txt45TotalFact").value);

    jsonObject.carg_otros_ind = parseInt(document.getElementById("txtOtroInd").value);
    jsonObject.carg_otros_agr = parseInt(document.getElementById("txtOtroAgr").value);
    jsonObject.tot_carg_otro = parseInt(document.getElementById("txtOtroTotalFact").value);

    jsonObject.estado_turno = "E";
    var tarDom = document.getElementById("txtTarDom").innerText.split("-");
    jsonObject.codi_inve_tari_dom15 = (tarDom[0].trim() == 0) ? 189 : tarDom[0].trim();
    var tarInd = document.getElementById("txtTarInd").innerText.split("-");
    jsonObject.codi_inve_tari_ind = (tarInd[0].trim() == 0) ? 41 : tarInd[0].trim();
    return jsonObject;
}

/* Agrega tarifa en label de informacion de turnos */
function establecerSeleccionado(objSelect, idLabel){
    var selected = objSelect.options[objSelect.selectedIndex];
    if(selected){
        document.getElementById(idLabel).textContent = selected.text;
    }else{
        //alert("ATENCIÓN: \n\nNo existe tarifa/destino en Domestico/Industrial para este distribuidor.\n\n");
    }
};

/* Limpia campos de informacion de turno */
function limpiarCamposInfo(){
    document.getElementById('txtCodTran').textContent = "-";
    document.getElementById('txtNomTran').textContent = "-";
    document.getElementById('txtDniTran').textContent = "-";
    document.getElementById('txtMarcVehi').textContent = "-";
    document.getElementById('txtPlacVehi').textContent = "-";
};

/* Limpia campos de tarifa en informacion de turno */
function limpiarLabelsTarifa(){
    document.getElementById('txtTarDom').textContent = '-';
    document.getElementById('txtTarInd').textContent = '-';
};

/* Construye pdf con datos de turno */
function construirPdf(turno){
    var imgData = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/7AARRHVja3kAAQAEAAAAZAAA/9sAQwACAQECAQECAgICAgICAgMFAwMDAwMGBAQDBQcGBwcHBgcHCAkLCQgICggHBwoNCgoLDAwMDAcJDg8NDA4LDAwM/9sAQwECAgIDAwMGAwMGDAgHCAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgARACgAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A/fyiiigAooooAKKKKACiiigAooooAKKKKAGiVTIU3LvUBiueQDnBx+B/KnV4v8SfiVH4V/bL+Ftha3EbQ+LtO1zSbxUkyrSWwtbiLcBxuT98Bnkec3qa9orqxGFlShTm9px5l/4FKP5xZ5+BzGGJq16Md6M+R+rhCa/8lmvncKKKK5T0AooooAKKKKACuP8Ah18fPCHxa8UeItF8O65a6pqnhO4+yatbxq6tZy75E2ncoDfNFIMqSMr16V2FfDv/AAS8/wCTsv2h/wDsNP8A+l99Xt5dllPEYLFYmbadJRats+aSi7/J9LHyeecQVsFm2X4CnFOOJlUjJu90o03NW1tutbp6H3FXF/F79ojwT8BLGO48YeJdL0NZhmKOeTdPMM4ykSguw9wpFVP2ofjnB+zh8CfEPjCaGO5k0u3/ANFt3batxcOwSJCeuC7DOOcA18rfsY/sIQ/tFaWvxc+NE154q1rxWftljYXUjpDHbnOx5FBBIYYKRjCKm3g543yrKcPPDSzDMJuNGL5UopOU5WvaN9FZatvRHHxHxLjqWPp5LklKNTEzi5tzbVOnC9uadvebb0jGOrs3dJHvPgn/AIKPfBbx/rMen2PjqxhupnEcYvraeyV2JwAHmRV5PvXtsUqzxK8bK6OAyspyGB6EGvD/AIq/8E5Pg/8AFHw1LY/8IXo/h+4KMsN7olslhPAx/i/dgK+PSRWHXivG/wBgf4ieJv2cf2ita/Z48aag2p29jC114ZvXyPMjCiXykHJ2PEWcKWIjaKRATxjqqZVl+Lw1SvlUp81Nc0oTs249ZRcbJ26ppO2p5+H4kzvLMfQwXEcKbp15ckKtLmUVUauoThNtpys1GSk02rNK+n2tXH6f8e/CGq/F288B2+uWkni7T7cXVxpgDeZHGQrZzjaTtdDgHOGziuwr4d+G3/Ka3xp/2Bz/AOkdpXnZNllPGRxEqja9nTlNW6tNKz8tT2+KuIK2V1MDCjFS9vXhSle+kZRk21ZrX3VvdeR9r69r1l4X0W61LUrq3sdPsYmnuLieQRxwRqMszMeAABnJrC+E/wAa/Cnx00CbVPCOu2OvWNvMbeWW2YnypAAdrAgEHBB5HINfOf8AwVm+KV7F8L/D/wAMtBZn8Q/ErUorPy0+99mV1yOOfnlMS+hXzK5P4CeG7P8AYN/4KDN8PYXkh8KfEjQrVrCWZjta8gjZeWJPzu6T8Z6zxjGCtejg+G4VcseKlJqrJSlCPRwg0pN9b6tr/Czwsy47q4biCOXQpp4aMqdOrO7vGpVUnTS6W0ipX1XPH5/cFfIf7Xv/AAVe0P4ZfEwfB74Q2MHxU+PmrGS2tNDtXP8AZfh+QD5rnVrpeIIIQS8iKTJhcEJuDV57/wAHCX/BQrWv2Kv2WdM8OeDdRudJ8cfE+eewttRtpDHcaRYQoGu7mJxyk37yGJGBDKZmdSGjFfG37NvwQm/ZN/ZW8F+AvC9is3xY+Pen2uueKNSjYfa0srx82Ojxv1SMriWbnLs53EqQB1cN8L/XIxr1tpN8qeitH4pSf8seys29Lrc6+NONqWS0Kk7OTglot3KTtCEf70n11tG7sz64/Y1nuvi9+2R4K0ux1q68YaL8G9Jv7jU/Eki7F17V74yG+vgB8qpcXUrtHGuAI4vl+UAD9D6+O7j40/A//gih8E/DGi/EDxKljrXjCdpJprawmurvVZkCCaURxqzLbwh0UZwAGGMu5z9RfCX4t+Gfjv8ADfR/GHg7W9P8ReGdftxdWGo2UvmQ3KEkHB7MGBVlOGVlZWAIIHBxNXjiK0auGg1QiuSEmrc1m23ta8pNuy+434Dy/E4PBz/tKaeKrSdWpFO/I5KMYwW7tCEYxvre27PP/wBqH/goD8G/2L9S0my+J3jzSfCd7rkbzWNvPHNNNPGhAZ9kSOyqCQNzAAnIBJBrsPgL+0H4L/ag+GVj4y8A+IrDxR4a1FnSC+tC20ujFXRlYBkZWGCrAEcccivwN/ai0zX/APgtn/wWY8UeHfCuq+Xpen295pGj33kfarex07S4Zm34VkytxelgGLcG8XlgoU++f8Gs/wC1FdeFPiH8Rfgdrkr2w1Bf+Ek0i1nba1tdw7be/hCkdSot2xkYMMhwSxI78ZwrTo5b9YjNutGMZSj0Slfy3Vtdej8jpwfE062YfV5QSpScoxl3lG1/l2/4c/aDVdVtdC0u5vr64t7Oys4mnuLieQRxQRqCzO7HAVQASSeABXx38Qf+DgL9lD4deIJNNm+JratNCSry6NoOoalbKQccTQwNG31ViK+bP+Dqr46+JfBvwS+GXgHS7y4sfDvji+v7vWxCxQ6gtmtv5Ns5B+aIvcGRkI5aKI9AQcP/AII1/wDBMX9kr9qj9i7Q9W1+DTfH3xC1KOT/AISCCTXp7e80OfewFuttDMhjVV2lWZSXB3ZKsBXPgslwkMuWZY9zcZOyULaWurtvTo/6Z04rOMTPMHl+DUbxV25X62sklr1P0n/ZZ/bc+Ff7avhm51b4Y+NNL8UwWJUXkMW+G7sS2dvnW8qrLHnBwWUA7TgnBr1Svxk/Y2/4I4fHz9h//grrZ634FhaL4P6Vqcgk8Q32pwEapoUyFmspYUcSyXCHYuTGqeZEkvA4r9m68nOsFhcNWSwdTnhJJra6v0duv3eh6WU4vE16TeLp8k4tp9nbqvIK+Hf+CXn/ACdl+0P/ANhp/wD0vvq+4q/PX4Z+AP2hv2WP2iPihrfhX4X2/iPT/F2sXMqSXN/bqkkQu55YZUInVgGWUkhgDyMgEYr2OGoxq4HG4XnjGU4w5eaSinaab1bS2R8Hx5Uq4fNspzBUalSnSqVHP2cJTaUqTim1FN2uz3L/AIK2+GbzxJ+xTrjWis39mXtpezgdREsoVj+G8H6A16d+x544074hfsueA9S0to/s39iW1syJ0hlhjWKSPH+y6MPwrxLSPjd+0h491e10PxP8CNDXw3rEy2WrM+qRbVtJCEmJHnnOIyxxg5xjFcMv7Ofxx/4J+eLNUuPg/HH48+H2oTG8fRLsh54CRg5j3I7SBQoEkRJcKu5DgV6EctjUy5ZVWrU41Yyc4Pni4yTSUouSbUZaJq9r7Hj1M9qUM9lxHhsLWnh5040qq9lONSDjJyjOMZJOcLSaly3cbJ+R94E4FfDPj3xDZ/ED/gsnoM2nXKx2fgHQW/ty83qsNqI4bl3LvnAUG7hjYnozEHoabqH7Un7U3x7spNB8M/CebwPNcp5c+q39vNbNbKeC0b3OxQR1+VZGHYZwa9I+Av8AwTT0f4dfs+eLvDfiDUm1bxR8QLRodZ1dQX8knLIsW7llSQ7yzYMjcnA2qsYHBUslhVqY+rHnqRdNRi1NpTspTlZ2SUb2V7tlZxm2J4rq4ehk+Gn7GjUjWlUqRlSjKVO7hThzJSblO3NK1oxT3bPpjSNYtPEGmW99YXVvfWd0glhuLeQSRTIeQyspIYH1HFfEvw24/wCC13jT/sD5/wDJO0qj8HdY/aV/Yr8EyeAYfhnaePdKsZpF0bU7W7HlxB2LYIDBjFuJYK6xsNxG7GMSfCb9lz4yeENH+KXxc13T1vPi54u02Sw0fS7a5hElkZ2RWmLF/LUxoqbEDnCxEEktiunL8rpZesVzYinKFWDpwanG8uaUbNpP3Ukryvaxw51xDis7llyhgq0amHqqvWTpySh7OE7xjJpKbk3aHLfm30PKfHv7a3hG4/4KX3PjzxgdTvvC/gsS6bocFlFHK3mQgxhyGdBtMrzyhgSc+WMEDIP29f8AgoL8P/2k9O8G6l4KTxJYeMvBuq/brG5vLaGONEO1mG5ZWbPmRQkADHynpX19/wAE2/2Urj9lz4Awwa1YxWni7XJmvNUAZJGt1B2wwb1yCEjAJAJAd5MEjFezfE/4d6b8Wvh7rHhrWII7jTdatJLWdXQNtDDAcZ6MpwykchlBGCAa6sRxNlOGzKn7KjKcaCVOMlOycVdN8vK073f2vevvqefgfD/ibMMgxH1jFxpTxjdadOVFuUZyalGLn7ROLjyxXwe5a1nY/Ev/AIOVNVn+Ofg39nb4rWMbN4b8SaNf6czoCyWd8TDK0DHGA5CTADOT9mk7KTXtHwH8X3n7Vnwv+Dvx8+E1jD4m8S/DvS9N0Hxn4XhkzfWN7ZJ5Yfy+pimjB2soPy7GAJD7fdvgx/wTm1b4z/slfEf4CfGbQ7qz8MnUVvPDmtW1zBJLFOd+Lm2wzFCjIsgDqAwndWBBYV+Yvxi/4IgftY/sSfEy41j4e2GueKLe1DR2niXwLqf2S+eEsPkktxKlypOFLIgkTI+82M16mCxuCozeAo1oJ0nJRbd4Tpz15W01rZpOzumuuqOzFZTjc5yynjMfQqRlUUJSSVqlKtSdudKS2um1dNOL9GfU3/BxV418A/HT9k/wX4q8QeG/FXgT4oaXqxs/DVhrUcNvd6pZyhTfgwpK7G3j2RMJZFTD7FX/AFjg+U/8Ex/21PEH7Hf/AARO+P8A4iGoXduyeJRoPgpicCDU720iEjQnrmMsbggZAKMccnPlX7P3/BDj9qH9tP4srq3xA0rXvBtjfyoNV8UeNLv7RqLRrwQlu0jXErhchVfYnI+YCvpz/grb/wAEt/i4fhj8F/gT+z78MNY8SfC/4e2E+pXOpf2pYQyanrNxI0bzXRmniJkC+Y5cIFH2pwMKqqvPOWW06dPKfaxa5+Z6+7FLWybb3elr31b0Wh9JhaWZylUzOVOXNycq0SlOT+01GyVvTolq9T5W/wCCJX/BRL4R/wDBM3xR418Q+OfC/jrXNf1yxtdJ0mXw/aWc8dlZozSTK/2i5hZWdxD93cCIhnoK4TWP25vCvw4/4KyH9oL4X6b4j0XwnJ4tXxHNpuqRQx3zRXWP7ViKxyyxgSmW7KAOQPMU/LgAfuH+zD/wRl+BPwe/Z+8JeG/E3wt8A+K/Eml6bEmr6vf6VHeTX94Runk8yRdxUyFtoOAF2gAAYr5s/wCC0/8AwRE0j4k/BHw/rf7PPwt0DT/GXh/UDHfaVoUdtpp1exlUhsh2jieSKQIwLMDtaQDPArnw/EeU18xm5Rkvarkcm1y22WnTb5XOqvw/mtHL6cYSjJ02ppKL5r7tXvrv21Pp/wD4KZab+zL8f/2dtD0T48+L/D2g+HfEMkep+G9Sm1VLC+hm8vC3Vo7ZPEcuG3K0e2TDqQcV+dnxj/4NZ/iF4QuW1r4R/FfQNbkhTzbSPV45tIuyMZAS5txKpYjODtRSSMlRkif4zf8ABIL4/ftff8E2/hLcav4Ul0X4xfBeG98K/wBgapqNpu8SaGJN9pJFLHI0Uc0SbIgkjgSBJGLJ8gON8EP+CiP/AAUI/Zf+EVh8OYvgP4k8QxeG7YaXp9/q3w41i8vLaGJRGiCa1dILhUVQFfDZAyWcVyZbRxOGocmWYqLalJSjJx5bX0a8mtXZ6+qaOzMalDEV75jhpW5U4yipX807a6P+tUYH/BMz/gqR8ev2Pv26/D/wN+LGr6/4m0LUfE9t4G1DSNauVv73QL2e4S0glt7osXaNZXiyvmPEYmZkGdpP74V+Mv8AwS5/4I3fF/4t/tj2/wC0R+0fZz6LcWus/wDCWWul3rQ/2lrOqF/NhmmhiJW1hhk2SLEdrho412Iqmv2arwuLqmDnio/VuXm5VzuPw83l39fv1PZ4Xp4qGFksTzW5ny83xcvS4UUUV8mfShRRRQAV8G6f+0f8cPjTZfFj4peHfiZ8P/AXhT4X+PbvwrZ+E/E1hFb6XfWenzxwXU2pak2Z7Wacs7R+WoSICLcsu8kfeVeC+Of+CZvwZ+IvxdvPGmq+FrqXUNU1K21rU7CLWr6DRdYv7fb5F3d6ckwtLiZCiHfJExJRSckA16GX4ihTcvbK97W0Uuuqs2rXXXddNzkxVKrO3s3tfq10027dup5H8R/+Ctqaf8dPE3wzsfCbWGpRjxFpmk6wmt2eoSQahpemz3267tIi32eKRIHMQkkMrBQXijDA1zvwl/4LF32n/s9eEfEniTwPrfiTSfDvhPwhffE3xja3FrZ2+jX2tWFncBobMnfcIgu4pZRHt8tJVCiQgqPeIv8AgmJ8HIvG7a8NB1o3S6rqutW9ufEeomxsbrVIbmHUZILbz/Ji+0LdzlwiAFmDcEAiFP8Aglh8EYdT8O3EfhW+ih8N2GjaZHYx65frp+pQ6OqrpgvrYTeVetbBF2NcLIflGScCvRWIyrlUfZy2V/W7v9rS626J62a35PY47Vqavd/d06d/nbqmWv2Vv2z9Z/aa8TeNJJPhtq3hfwX4R1PVdETxJeatazR6neadfy2k6R26HzlTEZcOwA3LIn8Ks/zqn/BY3VtV8TeCvH194L1vwf8ABTVPBfi7xnFNPLZ3t74osdMFmIHVEbfaTZlJELHawuEy+VYL9u/CL4M+HfgX4TuND8M2T2Om3eqahrMsT3Ek5e6vrya8uX3SMxw888rBc4UMAAAAK8d8H/8ABKP4F+CNWlubPwjeTwNpmqaJFp97rt/eabZadqRDXtlBayzNDDbyMN3lxoqqeRjisKGIy9Tm6lN2+zbXSzT3lo72d9UtbJaG1SjirRUZ+vrpbZbb9vVln9hv9vCP9sq68UWM3hG68L6l4XisLqRotRi1XTrqG8SR41ju4gENxGYnSaHGY224Lq6ufk34C/8ABWX4k/Ej4LfsxtqWk+KdP1vxx43g0vxV4nvfDEVv4f1mxK6gZIrWYMVWTEMRBVQcQyHPUH7s/Z+/ZU8I/szx6h/wjP8Awkk82pxW1tNca14hvtanWC3V1ggR7uWRo4oxI+EQgZYk5JzWT4c/YT+GPhP4ZfDfwfYeH5ofD/wl1lNf8MW51G5ZrC8QXAWRnMheUf6TN8shZSWGR8oxUMXl8JztTbi2uXy92Se7f2mna/TyRjUw+MlGFp2aTv56xa2S6Jr5nz0f+C22keH/AIcQ+NPFHwu8YeHfCfirwreeLvAt019Z3Vx4rtbee1gWF4UfNnPK19aMiyMybJss6lWUaPiL/grhqngW88YaH4o+Ek/hXxh4PvtB0+6tNV8Y6bb6Zv1aK8mik+2k8xoloVIjjkmeSQKsRwWrqvGn/BJP4YQ/CXxrofgvTI9F1LxJ4cvfDeltrNxda7pXh21upBNLb2lhcTGK3t3lVGaO38o/Iu0qUXHG/s8f8EcdE8Na34m1/wCImqSal4g1jVNJ1PS5PDes6zZSaBJp1pcWkUsV9NeS30kskd3cK5ebbsZUC4QE9UZ5O4Sm4vyWt3qrfatte+r11VtE8ZRzJTjFNW6vTTR+Xe1tFpvfdXvg5/wVwu/2iPEnw30bwT8H/E2san460S71++hm1mysxoFtZ6w2lXhdpWHmmOVdyKnzSKRwpzj7Mrx/4EfsGfC39mrxRpus+DfDs+l6ho+maho9nI+p3V0IbW+1FtSuY8SyMDuumLhjllB2gheK9grx8fUw0p/7LFxjrvvu7dXsrHpYSFeMP9old+W23outwooorhOoKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD/2Q==';
    var doc = new jsPDF('p', 'mm', [77.5, 149]);
    doc.addImage(imgData, 'JPEG', 05, 03, 35, 17);
    doc.setFontSize(09);
    doc.setFontType("bold");
    doc.text(62, 05, "TURNO");
    doc.rect(59,06,16,11);
    doc.text(12, 24, turno.nombre_sucursal);
    doc.setFontSize(08);
    doc.text(18, 29, "CONTROL DE E/S DE CILINDROS");
    doc.rect(08, 32, 65, 05);
    doc.text(09, 36, "Orden No. : ");
    doc.text(08, 41, "Fecha  :");
    doc.text(47, 41, "Hora:");
    doc.text(08, 45, "Distrib.:");
    doc.text(08, 49, "Chofer :");
    doc.text(08, 53, "Cédula :");
    doc.text(08, 57, "Vehíc.  :");
    doc.text(08, 61, "Placa   :");
    doc.rect(08, 63, 65, 20);//Bycss: add Rect
    doc.line(23, 67, 23, 83);//Bycss: Add Vertical
    doc.line(61, 67, 61, 83);//Bycss: Add Vertical
    doc.line(08, 67, 73, 67);//Bycss: Add linea horizontal
    doc.line(23, 71, 73, 71);//Bycss: Add linea horizontal
    doc.line(23, 75, 73, 75);//Bycss: Add linea horizontal
    doc.line(23, 79, 73, 79);//Bycss: Add linea horizontal
    doc.text(15, 66, "CANTIDAD CILINDROS QUE INGRESAN");
    doc.setFontType("bold");
    doc.setFontSize(07);
    doc.text(26, 70, "Llenos     Vacios     Fugas           Total");
    doc.text(10, 74, "Cil. 15kg");
    doc.text(10, 78, "Cil. 45kg");
    doc.text(10, 82, "Cil. Otro");
    doc.text(42, 86, "Total Cilindros:");
    doc.rect(08, 89, 65, 28);//Bycss: add rect
    doc.line(23, 93, 23, 109);//Bycss: Add V.
    doc.line(61, 93, 61, 109);//Bycss: Add V.
    doc.line(08, 93, 73, 93);//Bycss: Add linea horizontal
    doc.line(23, 97, 73, 97);//Bycss: Add linea horizontal
    doc.line(23, 101, 73, 101);//Bycss: Add linea horizontal
    doc.line(23, 105, 73, 105);//Bycss: Add linea horizontal
    doc.line(08, 109, 73, 109);//Bycss: Add linea horizontal
    doc.setFontType("bold");
    doc.text(11, 92, "SALIDA DE CILINDROS | CARGAS A FACTURAR");
    doc.text(24, 96, "Domést.     Indust.     Agríc.           Total");
    doc.text(09, 100, "Cil. 15kg");
    doc.text(09, 104, "Cil. 45kg");
    doc.text(09, 108, "Cil. Otros");
    doc.text(09, 112, "Id. Destino:");//Bycss: add
    doc.line(08, 129, 35, 129);//Bycss: Add linea horizontal
    doc.line(44, 129, 74, 129);//Bycss: Add linea horizontal
    doc.text(13, 132, "Coord. Plataf.");
    doc.text(50, 132, "Distrib. / Transp.");
    doc.setFontType("italic");
    doc.text(15, 138, "Por favor entregue correctamente sus datos,");
    doc.text(21, 141, "caso contrario perderá su turno");
    doc.setFontSize(27);
    doc.setFontType("normal");
    (turno.turno_ingr < 10) ? doc.text(64, 15, turno.turno_ingr.toString()) : doc.text(62, 15, turno.turno_ingr.toString());
    doc.setFontSize(08);
    doc.setFontType("bold");
    doc.text(35, 36, turno.num_turno_pape.toString());//Id Orden / papeleta
    doc.setFontSize(08);
    doc.setFontType("bold");
    doc.text(57, 41, turno.hora_entrd);
    doc.text(21, 41, turno.fecha_entrd);
    doc.text(21, 45, turno.nomb_inve_clie);
    var chofer = (turno.chof_inve_trsp.length > 25) ? turno.chof_inve_trsp.substr(0,29) + "..." : chofer = turno.chof_inve_trsp;
    doc.text(21, 49, chofer);
    doc.text(21, 53, turno.iden_inve_trsp);
    doc.text(21, 57, turno.marc_inve_trsp);
    doc.text(21, 61, turno.plac_inve_trsp);

    /* Tabla ingreso de cilindros */
    doc.text(28, 74, turno.cil_llen_15kg.toString());
    doc.text(39, 74, turno.cil_vaci_15kg.toString());
    doc.text(50, 74, turno.cil_fuga_15kg.toString());
    doc.text(65, 74, turno.tot_cil_15kg.toString());
    doc.text(28, 78, turno.cil_llen_45kg.toString());
    doc.text(39, 78, turno.cil_vaci_45kg.toString());
    doc.text(50, 78, turno.cil_fuga_45kg.toString());
    doc.text(65, 78, turno.tot_cil_45kg.toString());
    doc.text(28, 82, turno.cil_llen_otro.toString());
    doc.text(39, 82, turno.cil_vaci_otro.toString());
    doc.text(50, 82, turno.cil_fuga_otro.toString());
    doc.text(65, 82, turno.tot_cil_otro.toString());
    doc.text(65, 86, turno.tot_cil_ingr.toString());

    /* Tabla cargas a facturar */
    doc.text(27, 100, turno.carg_dom_15kg.toString());
    doc.text(40, 100, turno.carg_ind_15kg.toString());
    doc.text(50, 100, turno.carg_agr_15kg.toString());
    doc.text(65, 100, turno.tot_carg_15kg.toString());
    doc.text(27, 104, "-");//Bycss: add
    doc.text(40, 104, turno.carg_ind_45kg.toString());
    doc.text(50, 104, turno.carg_agr_45kg.toString());
    doc.text(65, 104, turno.tot_carg_45kg.toString());
    doc.text(27, 108, "-");//Bycss: add
    doc.text(40, 108, turno.carg_otros_ind.toString());
    doc.text(50, 108, turno.carg_otros_agr.toString());
    doc.text(65, 108, turno.tot_carg_otro.toString());

    doc.text(27, 112, turno.codi_inve_tari_dom15);//Bycss: add
    doc.text(38, 112, turno.codi_inve_tari_ind);//Bycss: add
    doc.text(50, 112, "0");//Bycss: add
    doc.text(12, 116, "Destino:");//Bycss: add
    doc.line(08, 113, 73, 113);//Bycss: Add linea horizontal
    var tarif_dome = (turno.nombre_tarifa_dom.length > 14) ? turno.nombre_tarifa_dom.substr(0,24) + "..." : turno.nombre_tarifa_dom;
    doc.text(27, 116, tarif_dome);//Bycss: add
    doc.autoPrint();
    return doc.output('datauristring');
}