/* scripts js stock_disponible.js */

/**
* Convierte string a entero
* @param {string} valor: cadena a transformar
* @return {integer} valor entero
*/
function converInt(valor){
    return typeof valor === 'string' ?
    valor.replace(/[\$,]/g, '')*1 :
    typeof valor === 'number' ?
    valor : 0;
};

/**
* Retorna un numero formateado con decimales y comas para miles
* @param  {int} numero: valor entero
* @param  {string} dia: valor para dividir numero por dias (exclusivo administrador del tiempo)
* @return  {float} num_for: valor formateado
*/
function formatNumero(numero){
    num_for = parseFloat(0.00);
    num_for = Number(parseFloat(numero).toFixed(2)).toLocaleString('en', {minimumFractionDigits: 0});
    return num_for;
};

/**
* Permite agregar color a texto segun condiciones
* @param  {object} row: fila de lista
* @param  {object} data: valor limpio a insertar en columna
* @return  {boolean} opcion: booleano para condicionar si mostrar solo fila de porcentaje o las demas
*/
function colorFila(row, data, opcion){
    if(opcion==true){
        if(row.CUPO === 0){
            return "<span class='error-distribuidor'>(N/A)</span>";
        }
        if(row.SALDO_REAL <= 0 && row.CUPO > 0){
            return "<span class='badge badge-pill badge-danger'>" + row.PORC_DESPACHO + "%</span>";
        }else if( row.SALDO_REAL <= ((row.CUPO * 15)/100) && row.SALDO_REAL > 0 ){
            return "<span class='badge badge-pill badge-warning'>" + row.PORC_DESPACHO + "%</span>";
        }else {
            return "<span class='badge badge-pill badge-success'>" + row.PORC_DESPACHO + "%</span>";
        }
    }else{
        if(row.CUPO === 0){
            return "<span class='error-distribuidor'>" + data + "</span>";
        }
        if(row.SALDO_REAL <= 0 && row.CUPO > 0){
            return "<span class='text-danger font-weight-bold'>" + data + "</span>";
        }else if( row.SALDO_REAL <= ((row.CUPO * 15)/100) && row.SALDO_REAL > 0 ){
            return "<span class='text-warning font-weight-bold'>" + data + "</span>";
        }else {
            return "<span class='text-success font-weight-bold'>" + data + "</span>";
        }
    }
};