/* scripts js stock_disponible.js */

/**
* Convierte string a entero
* @param {string} valor: cadena a transformar
* @return {integer} valor entero
*/
function converInt(valor){
    return typeof valor === 'string' ?
    valor.replace(/[\$,]/g, '')*1 :
    typeof valor === 'number' ?
    valor : 0;
};

/**
* Retorna un numero formateado con decimales y comas para miles
* @param  {int} numero: valor entero
* @param  {string} dia: valor para dividir numero por dias (exclusivo administrador del tiempo)
* @return  {float} num_for: valor formateado
*/
function formatNumero(numero){
    num_for = parseFloat(0.00);
    num_for = Number(parseFloat(numero).toFixed(2)).toLocaleString('en', {minimumFractionDigits: 0});
    return num_for;
};

/**
* Permite agregar color a texto segun condiciones
* @param  {object} data: valor limpio a insertar en columna
* @return  {string} html para insertar en celda
*/
function colorFila(data){
    if(data > 0){
        return "<span class='text-success font-weight-bold'>" + data + " <i class='bi bi-caret-up-fill'></i></span>";
    } else if(data < 0){
        return "<span class='text-danger font-weight-bold'>" + data + " <i class='bi bi-caret-down-fill'></i></span>";
    } else {
        return "<span class='font-weight-bold'> " + data + "</span>";
    }
};