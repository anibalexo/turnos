
/**
* Genera codigo HTML en string para input minimo dosmestico
* @param {number} data: campo minimo domestico en datatables
* @param {array} row: fila de tabla con los campos
* @return {string} html: codigo html
*/
function generarHTMLInputMin(data, row){
    var htmlInput = "<div class='form-inline text-right'><input  class='form-control form-control-sm' id='txt_min" + row.pk + "' size='8' type='number' value='" + data + "'>";
    return htmlInput
};

/**
* Genera codigo HTML en string para input de cupo o meta
* @param {number} data: campo CUPO/META en datatables
* @param {array} row: fila de tabla con los campos
* @return {string} html: codigo html
*/
function generarHTMLInputCupoMeta(data, row){
    var htmlInput = "<div class='form-inline text-right'><input  class='form-control form-control-sm' id='txt" + row.pk + "' size='8' type='number' value='" + data + "'>";
    return htmlInput
};

/**
* Genera codigo HTML en string para acciones de tabla
* @param {number} data: campo CUPO en datatables
* @param {array} row: fila de tabla con los campos
* @return {string} html: codigo html
*/
function generarHTMLAcciones(data, row){
    var htmlButton = "<div class='form-inline text-right'><button type='button' class='btn btn-outline-secondary btn-sm ml-2' " +
                     "onclick='actualizarCupoMeta(" + row.pk + "," + row.codi_inve_clie + "," + row.codi_inve_tari + ")'>Actualizar</button></div>";
    var htmlBloqueado = "<div class='form-inline text-right'><span class='text-danger small ml-2'>No permitido</span></div>";
    return (row.editable) ? htmlButton : htmlBloqueado;
};

/**
* Genera objeto json con atributos de cupo
* @param {number} pkCupo: id de registro cupo
* @param {number} periodo: id de periodo cupo
* @param {number} cliente: id de cliente
* @param {number} tarifa: id de tarifa
* @param {number} cupo_nuevo: valor nuevo en cupo
* @return {Object} jsonObject: objecto json
*/
function generarJsonDatosCupo(pkCupo, periodo, cliente, tarifa, cupo_nuevo, min_dom){
    var jsonObject = new Object();
    jsonObject.pk = pkCupo;
    jsonObject.inve_codi_cupo = periodo;
    jsonObject.codi_inve_clie = cliente;
    jsonObject.codi_inve_tari = tarifa;
    jsonObject.cupo_cant_prod = cupo_nuevo;
    jsonObject.saldo_cant_prod = cupo_nuevo;
    jsonObject.nume_inve_prod_equ0 = min_dom;
    return jsonObject;
};

/**
* Genera objeto json con atributos de meta
* @param {number} pkMeta: id de registro meta
* @param {number} periodo: id de periodo meta
* @param {number} cliente: id de cliente
* @param {number} tarifa: id de tarifa
* @param {number} metaNueva: valor nuevo en meta
* @return {Object} jsonObject: objecto json
*/
function generarJsonDatosMeta(pkMeta, periodo, cliente, tarifa, metaNueva, min_dom){
    var jsonObject = new Object();
    jsonObject.pk = pkMeta;
    jsonObject.inve_codi_meta = periodo;
    jsonObject.codi_inve_clie = cliente;
    jsonObject.codi_inve_tari = tarifa;
    jsonObject.cupo_cant_prod = metaNueva;
    jsonObject.saldo_cant_prod = metaNueva;
    jsonObject.nume_inve_prod_equ0 = min_dom;
    return jsonObject;
};