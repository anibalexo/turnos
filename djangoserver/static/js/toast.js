
/**
* Muestra mensaje toast
* @param {string} mensaje: texto a mostrar en toast
* @param {string} tipo: string error y success
*/
function mostrarToast(mensaje, tipo){
    var x = document.getElementById("toastmsj");
    x.innerHTML = '<i class="bi bi-info-circle mr-2"></i>' + mensaje;
    if(tipo === 'error'){
        x.classList.add("toastmsj-error");
    } else if(tipo === 'success'){
        x.classList.add("toastmsj-success");
    }
    x.classList.add("show");
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}